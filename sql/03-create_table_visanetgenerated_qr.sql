DROP TABLE IF EXISTS  `mobilecard`.`visanet_generated_qr`;

CREATE TABLE `mobilecard`.`visanet_generated_qr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) DEFAULT NULL,
  `id_application` bigint(20) DEFAULT NULL,
  `id_country` bigint(20) DEFAULT NULL,
  `id_language` varchar(20) DEFAULT NULL,
  `id_business` bigint(20) DEFAULT NULL,
  `qr_id` varchar(1024) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` decimal(13,4) DEFAULT '0.0000',
  `comision` decimal(13,4) DEFAULT '0.0000',
  `expiration_date` datetime DEFAULT NULL,
  `generated_qr` tinyint(4) DEFAULT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `visanet_qr_response` blob,
  `status` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_QR_ID` (`qr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
