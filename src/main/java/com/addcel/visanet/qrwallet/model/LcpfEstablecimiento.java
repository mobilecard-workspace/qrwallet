package com.addcel.visanet.qrwallet.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="LCPF_establecimiento")
@Access(AccessType.FIELD)
public class LcpfEstablecimiento  implements Serializable {
   
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name="usuario")
    private String usuario;
    
    @Column(name="pass") 
    private String pass;    
    
    @Column(name="nombre_establecimiento") 
    private String nombreEstablecimiento;
    
    @Column(name="rfc") 
    private String rfc;
    
    @Column(name="razon_social") 
    private String razonSocial;
    
    @Column(name="representante_legal") 
    private String representanteLegal;
    
    @Column(name="direccion_establecimiento") 
    private String direccionEstablecimiento;
    
    @Column(name="email_contacto") 
    private String emailContacto;
    
    @Column(name="telefono_contacto")
    private String telefonoContacto;
    
    @Column(name="id_banco") 
    private String idBanco;
    
    @Column(name="cuenta_clabe") 
    private String cuentaClabe;
    
    @Column(name="nombre_prop_cuenta") 
    private String nombrePropCuenta;
    
    @Column(name="estatus") 
    private Long status;
    
    @Column(name="id_account") 
    private Long idAccount;
    
    @Column(name="comision_fija")
    private BigDecimal comisionFija;
    
    @Column(name="comision_porcentaje") 
    private BigDecimal comisionPorcentaje;
    
    @Column(name="urlLogo") 
    private String urlLogo;
    
    @Column(name="cuenta_tipo") 
    private String cuentaTipo;
    
    @Column(name="codigo_banco")
    private String codigoBanco;
    
    @Column(name="id_aplicacion") 
    private Long idAplicacion;
    
    @Column(name="tipo_comision") 
    private Long tipoComision;
    
    @Column(name="img_ine") 
    private String  imgIne;
    
    @Column(name="img_domicilio")
    private String imgDomicilio;
    
    @Column(name="fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @Column(name="representante_nombre") 
    private String representanteNombre;
    
    @Column(name="representante_paterno")
    private String representantePaterno;
    
    @Column(name="representante_materno")
    private String representanteMaterno;
    
    @Column(name="representante_curp") 
    private String representanteCurp;
    
    @Column(name="calle") 
    private String calle;
    
    @Column(name="colonia") 
    private String colonia;
   
    @Column(name="cp")
    private String cp;
    
    @Column(name="municipio") 
    private String municipio;
    
    @Column(name="id_estado") 
    private Long idEstado;
    
    @Column(name="sms_code") 
    private String smsCode;
    
    @Column(name="tipo_documento")
    private String tipoDocumento;
    
    @Column(name="numero_documento") 
    private String numeroDocumento;
    
    @Column(name="fecha_nacimiento") 
    private String fechaNacimiento;
    
    @Column(name="nacionalidad")
    private String nacionalidad;
    
    @Column(name="distrito") 
    private String distrito;
    
    @Column(name="provincia")
    private String provincia;
    
    @Column(name="departamento") 
    private String departamento;
    
    @Column(name="centro_laboral") 
    private String centroLaboral;
    
    @Column(name="ocupacion") 
    private String ocupacion;
    
    @Column(name="cargo") 
    private String cargo;
    
    @Column(name="institucion") 
    private String institucion;
    
    @Column(name="cci") 
    private String cci;
    
    @Column(name="id_pais") 
    private String idPais;
    
    @Column(name="digito_verificador") 
    private String digitoVerificador;
    
    @Column(name="jumio_status") 
    private Long jumioStatus;
    
    @Column(name="jumio_reference") 
    private String jumioReference;
    
    public LcpfEstablecimiento() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getNombreEstablecimiento() {
        return nombreEstablecimiento;
    }

    public void setNombreEstablecimiento(String nombreEstablecimiento) {
        this.nombreEstablecimiento = nombreEstablecimiento;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRepresentanteLegal() {
        return representanteLegal;
    }

    public void setRepresentanteLegal(String representanteLegal) {
        this.representanteLegal = representanteLegal;
    }

    public String getDireccionEstablecimiento() {
        return direccionEstablecimiento;
    }

    public void setDireccionEstablecimiento(String direccionEstablecimiento) {
        this.direccionEstablecimiento = direccionEstablecimiento;
    }

    public String getEmailContacto() {
        return emailContacto;
    }

    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }

    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    public void setTelefonoContacto(String telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }

    public String getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(String idBanco) {
        this.idBanco = idBanco;
    }

    public String getCuentaClabe() {
        return cuentaClabe;
    }

    public void setCuentaClabe(String cuentaClabe) {
        this.cuentaClabe = cuentaClabe;
    }

    public String getNombrePropCuenta() {
        return nombrePropCuenta;
    }

    public void setNombrePropCuenta(String nombrePropCuenta) {
        this.nombrePropCuenta = nombrePropCuenta;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Long idAccount) {
        this.idAccount = idAccount;
    }

    public BigDecimal getComisionFija() {
        return comisionFija;
    }

    public void setComisionFija(BigDecimal comisionFija) {
        this.comisionFija = comisionFija;
    }

    public BigDecimal getComisionPorcentaje() {
        return comisionPorcentaje;
    }

    public void setComisionPorcentaje(BigDecimal comisionPorcentaje) {
        this.comisionPorcentaje = comisionPorcentaje;
    }

    public String getUrlLogo() {
        return urlLogo;
    }

    public void setUrlLogo(String urlLogo) {
        this.urlLogo = urlLogo;
    }

    public String getCuentaTipo() {
        return cuentaTipo;
    }

    public void setCuentaTipo(String cuentaTipo) {
        this.cuentaTipo = cuentaTipo;
    }

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public Long getIdAplicacion() {
        return idAplicacion;
    }

    public void setIdAplicacion(Long idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public Long getTipoComision() {
        return tipoComision;
    }

    public void setTipoComision(Long tipoComision) {
        this.tipoComision = tipoComision;
    }

    public String getImgIne() {
        return imgIne;
    }

    public void setImgIne(String imgIne) {
        this.imgIne = imgIne;
    }

    public String getImgDomicilio() {
        return imgDomicilio;
    }

    public void setImgDomicilio(String imgDomicilio) {
        this.imgDomicilio = imgDomicilio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRepresentanteNombre() {
        return representanteNombre;
    }

    public void setRepresentanteNombre(String representanteNombre) {
        this.representanteNombre = representanteNombre;
    }

    public String getRepresentantePaterno() {
        return representantePaterno;
    }

    public void setRepresentantePaterno(String representantePaterno) {
        this.representantePaterno = representantePaterno;
    }

    public String getRepresentanteMaterno() {
        return representanteMaterno;
    }

    public void setRepresentanteMaterno(String representanteMaterno) {
        this.representanteMaterno = representanteMaterno;
    }

    public String getRepresentanteCurp() {
        return representanteCurp;
    }

    public void setRepresentanteCurp(String representanteCurp) {
        this.representanteCurp = representanteCurp;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public Long getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Long idEstado) {
        this.idEstado = idEstado;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCentroLaboral() {
        return centroLaboral;
    }

    public void setCentroLaboral(String centroLaboral) {
        this.centroLaboral = centroLaboral;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getCci() {
        return cci;
    }

    public void setCci(String cci) {
        this.cci = cci;
    }

    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    public String getDigitoVerificador() {
        return digitoVerificador;
    }

    public void setDigitoVerificador(String digitoVerificador) {
        this.digitoVerificador = digitoVerificador;
    }

    public Long getJumioStatus() {
        return jumioStatus;
    }

    public void setJumioStatus(Long jumioStatus) {
        this.jumioStatus = jumioStatus;
    }

    public String getJumioReference() {
        return jumioReference;
    }

    public void setJumioReference(String jumioReference) {
        this.jumioReference = jumioReference;
    }

    
}

