package com.addcel.visanet.qrwallet.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="tarjetas_usuario")
@Access(AccessType.FIELD)
public class TarjetaCredito implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "idtarjetasusuario")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_aplicacion")
    private Long idAplicacion;

    @Column(name = "idusuario")
    private Long idUsuario;

    @Column(name = "numerotarjeta")
    private String numero;

    @Column(name = "vigencia")
    private String vigencia;

    @Column(name = "estado")
    private Long estado;

    @Column(name = "idbanco")
    private Long idBanco;

    @Column(name = "idfranquicia")
    private Long idFranquicia;

    @Column(name = "idtarjetas_tipo")
    private Long idTarjetasTipo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecharegistro")
    private Date FechaRegistro;

    @Column(name = "ct")
    private String ct;

    @Column(name = "nombre_tarjeta")
    private String nombreTarjeta;

    @Column(name = "usrdomamex")
    private String usrDomAmex;

    @Column(name = "usrcpamex")
    private String usrCpAmex;

    @Column(name = "mobilecard")
    private Long mobilecard;

    @Column(name = "previvale")
    private String previvale;

    @Column(name = "address")
    private String address;

    @Column(name = "id_profile_viamericas")
    private String idProfileViamericas;

    @Column(name = "act_type")
    private String actType;

    @Column(name = "clabe")
    private String clabe;

    @Column(name = "num_cuenta")
    private String numCuenta;

    @Column(name = "card_token")
    private String cardToken;

    @Column(name = "masked_pan")
    private String maskedPan;


    public TarjetaCredito() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAplicacion() {
        return idAplicacion;
    }

    public void setIdAplicacion(Long idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getVigencia() {
        return vigencia;
    }

    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public Long getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Long idBanco) {
        this.idBanco = idBanco;
    }

    public Long getIdFranquicia() {
        return idFranquicia;
    }

    public void setIdFranquicia(Long idFranquicia) {
        this.idFranquicia = idFranquicia;
    }

    public Long getIdTarjetasTipo() {
        return idTarjetasTipo;
    }

    public void setIdTarjetasTipo(Long idTarjetasTipo) {
        this.idTarjetasTipo = idTarjetasTipo;
    }

    public Date getFechaRegistro() {
        return FechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        FechaRegistro = fechaRegistro;
    }

    public String getCt() {
        return ct;
    }

    public void setCt(String ct) {
        this.ct = ct;
    }

    public String getNombreTarjeta() {
        return nombreTarjeta;
    }

    public void setNombreTarjeta(String nombreTarjeta) {
        this.nombreTarjeta = nombreTarjeta;
    }

    public String getUsrDomAmex() {
        return usrDomAmex;
    }

    public void setUsrDomAmex(String usrDomAmex) {
        this.usrDomAmex = usrDomAmex;
    }

    public String getUsrCpAmex() {
        return usrCpAmex;
    }

    public void setUsrCpAmex(String usrCpAmex) {
        this.usrCpAmex = usrCpAmex;
    }

    public Long getMobilecard() {
        return mobilecard;
    }

    public void setMobilecard(Long mobilecard) {
        this.mobilecard = mobilecard;
    }

    public String getPrevivale() {
        return previvale;
    }

    public void setPrevivale(String previvale) {
        this.previvale = previvale;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdProfileViamericas() {
        return idProfileViamericas;
    }

    public void setIdProfileViamericas(String idProfileViamericas) {
        this.idProfileViamericas = idProfileViamericas;
    }

    public String getActType() {
        return actType;
    }

    public void setActType(String actType) {
        this.actType = actType;
    }

    public String getClabe() {
        return clabe;
    }

    public void setClabe(String clabe) {
        this.clabe = clabe;
    }

    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public String getMaskedPan() {
        return maskedPan;
    }

    public void setMaskedPan(String maskedPan) {
        this.maskedPan = maskedPan;
    }
}