package com.addcel.visanet.qrwallet.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.addcel.visanet.qrwallet.model.mappings.LongMappingDTO;

@SqlResultSetMapping(
        name="mapIdUsuario",
        classes = {
            @ConstructorResult(
                targetClass=LongMappingDTO.class,
                columns= {
                       @ColumnResult(name="id_usuario", type=Long.class) 
                }
            )
        }
)
@Entity
@Table(name="t_bitacora")
@Access(AccessType.FIELD)
public class TBitacora implements Serializable {

    private static final long serialVersionUID = 1L;

    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id_bitacora")
    private Long id;
    
    @Column(name="id_usuario")
    private Long idUsuario;
    
    @Column(name="id_aplicacion")
    private Long idAplicacion;

    @Column(name="idioma")
    private String idioma;

    @Column(name="id_proveedor")
    private Long idProveedor;
    
    @Column(name="id_producto")
    private Long idProducto;
    
    @Temporal(TemporalType.DATE)
    @Column(name="bit_fecha")
    private Date fecha;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="bit_hora")
    private Date hora;
    
    @Column(name="bit_concepto")
    private String concepto;
    
    @Column(name="bit_cargo")
    private BigDecimal cargo;
    
    @Column(name="bit_comision")
    private BigDecimal comision;
    
    @Column(name="bit_ticket")
    private String ticket;
    
    @Column(name="bit_no_autorizacion")
    private String numeroAutorizacion;
    
    @Column(name="bit_codigo_error")
    private Integer codigoError;
    
    @Column(name="bit_card_id")
    private Integer cardId;
    
    @Column(name="bit_referencia")
    private String referencia;
    
    @Column(name="bit_status")
    private Integer status;
    
    @Column(name="imei")
    private String imei;
    
    @Column(name="destino")
    private String destino;
    
    @Column(name="tarjeta_compra")
    private String tarjetaCompra;

    @Column(name="tipo")
    private String tipo;
    
    @Column(name="software")
    private String software;
    
    @Column(name="modelo")
    private String modelo;
    
    @Column(name="wkey")
    private String wkey;
    
    @Column(name="pase")
    private Long pase;
    
    @Column(name="cx")
    private String cx;
    
    @Column(name="cy")
    private String cy;

    @Column(name="id_establecimiento")
    private Long idEstablecimiento;
    
    public TBitacora() {
        super();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdAplicacion() {
        return idAplicacion;
    }

    public void setIdAplicacion(Long idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public Long getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Long idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public BigDecimal getCargo() {
        return cargo;
    }

    public void setCargo(BigDecimal cargo) {
        this.cargo = cargo;
    }

    public BigDecimal getComision() {
        return comision;
    }

    public void setComision(BigDecimal comision) {
        this.comision = comision;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public Integer getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(Integer codigoError) {
        this.codigoError = codigoError;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getTarjetaCompra() {
        return tarjetaCompra;
    }

    public void setTarjetaCompra(String tarjetaCompra) {
        this.tarjetaCompra = tarjetaCompra;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getWkey() {
        return wkey;
    }

    public void setWkey(String wkey) {
        this.wkey = wkey;
    }

    public Long getPase() {
        return pase;
    }

    public void setPase(Long pase) {
        this.pase = pase;
    }

    public String getCx() {
        return cx;
    }

    public void setCx(String cx) {
        this.cx = cx;
    }

    public String getCy() {
        return cy;
    }

    public void setCy(String cy) {
        this.cy = cy;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public Long getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(Long idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }
}
