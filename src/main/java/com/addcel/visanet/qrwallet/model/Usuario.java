package com.addcel.visanet.qrwallet.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_usuarios")
@Access(AccessType.FIELD)
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Long id;

    @Column(name = "SMS_CODE")
    private String smsCode;

    @Column(name = "id_aplicacion")
    private Long idAplicacion;

    @Column(name = "usr_login")
    private String login;

    @Column(name = "usr_pwd")
    private String pwd;

    @Column(name = "usr_fecha_nac")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;

    @Column(name = "usr_telefono")
    private String telefono;

    @Column(name = "operador")
    private String operador;

    @Column(name = "usr_fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;

    @Column(name = "usr_nombre")
    private String nombre;

    @Column(name = "usr_apellido")
    private String apellido;

    @Column(name = "usr_direccion")
    private String direccion;

    @Column(name = "usr_tdc_numero")
    private String tdcNumero;

    @Column(name = "usr_tdc_vigencia")
    private String tdcVigencia;

    @Column(name = "id_banco")
    private Long idBanco;

    @Column(name = "id_tipo_tarjeta")
    private Long idTipoTarjeta;

    @Column(name = "id_proveedor")
    private Long idProveedor;

    @Column(name = "id_usr_status")
    private Long idStatus;

    @Column(name = "cedula")
    private String cedula;

    @Column(name = "tipocedula")
    private Long tipoCedula;

    @Column(name = "recibirSMS")
    private Long recibirSms;

    @Column(name = "idpais")
    private Long idPais;

    @Column(name = "gemalto")
    private Long gemAlto;

    @Column(name = "eMail")
    private String email;

    @Column(name = "imei")
    private String imei;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "software")
    private String software;

    @Column(name = "modelo")
    private String modelo;

    @Column(name = "wkey")
    private String wkey;

    @Column(name = "telefono_original")
    private String telefonoOriginal;

    @Column(name = "usr_materno")
    private String materno;

    @Column(name = "usr_sexo")
    private String sexo;

    @Column(name = "usr_tel_casa")
    private String telCasa;

    @Column(name = "usr_tel_oficina")
    private String telOficina;

    @Column(name = "usr_id_estado")
    private Long idEstado;

    @Column(name = "usr_ciudad")
    private String ciudad;

    @Column(name = "usr_calle")
    private String calle;

    @Column(name = "usr_num_ext")
    private Long numExt;

    @Column(name = "usr_num_interior")
    private String numInterior;

    @Column(name = "usr_colonia")
    private String colonia;

    @Column(name = "usr_cp")
    private String cp;

    @Column(name = "usr_dom_amex")
    private String domAmex;

    @Column(name = "usr_terminos")
    private String terminos;

    @Column(name = "num_ext_STR")
    private String numExtension;

    @Column(name = "id_ingo")
    private String idIngo;

    @Column(name = "usr_nss")
    private String usrNss;

    @Column(name = "id_sender")
    private String idSender;

    @Column(name = "usr_rfc")
    private String rfc;

    @Column(name = "usr_curp")
    private String curp;

    @Column(name = "usr_email")
    private String email2;

    @Column(name = "pb_token")
    private String pbToken;

    @Column(name = "usr_sms")
    private String sms;

    @Column(name = "jumio_status")
    private Long jumioStatus;

    @Column(name = "jumio_reference")
    private String jumioReference;

    @Column(name = "usr_nacionalidad")
    private String nacionalidad;

    @Column(name = "usr_distrito")
    private String distrito;

    @Column(name = "usr_provincia")
    private String provincia;

    @Column(name = "usr_departamento")
    private String departamento;

    @Column(name = "usr_centro_laboral")
    private String centroLaboral;

    @Column(name = "usr_ocupacion")
    private String ocupacion;

    @Column(name = "usr_cargo_publico")
    private String cargoPublico;

    @Column(name = "usr_institucion")
    private String institucion;

    public Usuario() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }

    public Long getIdAplicacion() {
        return idAplicacion;
    }

    public void setIdAplicacion(Long idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTdcNumero() {
        return tdcNumero;
    }

    public void setTdcNumero(String tdcNumero) {
        this.tdcNumero = tdcNumero;
    }

    public String getTdcVigencia() {
        return tdcVigencia;
    }

    public void setTdcVigencia(String tdcVigencia) {
        this.tdcVigencia = tdcVigencia;
    }

    public Long getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Long idBanco) {
        this.idBanco = idBanco;
    }

    public Long getIdTipoTarjeta() {
        return idTipoTarjeta;
    }

    public void setIdTipoTarjeta(Long idTipoTarjeta) {
        this.idTipoTarjeta = idTipoTarjeta;
    }

    public Long getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Long idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Long getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Long idStatus) {
        this.idStatus = idStatus;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Long getTipoCedula() {
        return tipoCedula;
    }

    public void setTipoCedula(Long tipoCedula) {
        this.tipoCedula = tipoCedula;
    }

    public Long getRecibirSms() {
        return recibirSms;
    }

    public void setRecibirSms(Long recibirSms) {
        this.recibirSms = recibirSms;
    }

    public Long getIdPais() {
        return idPais;
    }

    public void setIdPais(Long idPais) {
        this.idPais = idPais;
    }

    public Long getGemAlto() {
        return gemAlto;
    }

    public void setGemAlto(Long gemAlto) {
        this.gemAlto = gemAlto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getWkey() {
        return wkey;
    }

    public void setWkey(String wkey) {
        this.wkey = wkey;
    }

    public String getTelefonoOriginal() {
        return telefonoOriginal;
    }

    public void setTelefonoOriginal(String telefonoOriginal) {
        this.telefonoOriginal = telefonoOriginal;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTelCasa() {
        return telCasa;
    }

    public void setTelCasa(String telCasa) {
        this.telCasa = telCasa;
    }

    public String getTelOficina() {
        return telOficina;
    }

    public void setTelOficina(String telOficina) {
        this.telOficina = telOficina;
    }

    public Long getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Long idEstado) {
        this.idEstado = idEstado;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public Long getNumExt() {
        return numExt;
    }

    public void setNumExt(Long numExt) {
        this.numExt = numExt;
    }

    public String getNumInterior() {
        return numInterior;
    }

    public void setNumInterior(String numInterior) {
        this.numInterior = numInterior;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getDomAmex() {
        return domAmex;
    }

    public void setDomAmex(String domAmex) {
        this.domAmex = domAmex;
    }

    public String getTerminos() {
        return terminos;
    }

    public void setTerminos(String terminos) {
        this.terminos = terminos;
    }

    public String getNumExtension() {
        return numExtension;
    }

    public void setNumExtension(String numExtension) {
        this.numExtension = numExtension;
    }

    public String getIdIngo() {
        return idIngo;
    }

    public void setIdIngo(String idIngo) {
        this.idIngo = idIngo;
    }

    public String getUsrNss() {
        return usrNss;
    }

    public void setUsrNss(String usrNss) {
        this.usrNss = usrNss;
    }

    public String getIdSender() {
        return idSender;
    }

    public void setIdSender(String idSender) {
        this.idSender = idSender;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getPbToken() {
        return pbToken;
    }

    public void setPbToken(String pbToken) {
        this.pbToken = pbToken;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public Long getJumioStatus() {
        return jumioStatus;
    }

    public void setJumioStatus(Long jumioStatus) {
        this.jumioStatus = jumioStatus;
    }

    public String getJumioReference() {
        return jumioReference;
    }

    public void setJumioReference(String jumioReference) {
        this.jumioReference = jumioReference;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCentroLaboral() {
        return centroLaboral;
    }

    public void setCentroLaboral(String centroLaboral) {
        this.centroLaboral = centroLaboral;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public String getCargoPublico() {
        return cargoPublico;
    }

    public void setCargoPublico(String cargoPublico) {
        this.cargoPublico = cargoPublico;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }
}

