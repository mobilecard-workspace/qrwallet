package com.addcel.visanet.qrwallet.model.mappings;

import java.io.Serializable;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;

public class LongMappingDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long value;

    public LongMappingDTO(Long value) {
        super();
        this.value = value;
    }

    public LongMappingDTO() {

    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

}
