package com.addcel.visanet.qrwallet.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "visanet_transaction_callback")
@Access(AccessType.FIELD)
public class VisanetTransactionCallback implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "transaction_successful")
    private Boolean successful;

    @Column(name="merchant_id")
    private String merchantId;

    @Column(name="merchant_name")
    private String merchantName;

    @Column(name="currency_code")
    private String currencyCode;

    @Column(name="country_code")
    private String countryCode;

    @Column(name="data_map_id_unico")
    private String mapIdUnico;

    @Column(name="data_map_authorization_code")
    private String mapAuthorizationCode;

    @Column(name="data_map_card")
    private String mapCard;

    @Column(name="data_map_terminal")
    private String mapTerminal;

    @Column(name="data_map_transaction_date")
    private String mapTransactionDate;

    @Column(name="data_map_action_code")
    private String mapActionCode;

    @Column(name="data_map_status")
    private String mapStatus;

    @Column(name="data_map_action_description")
    private String mapActionDescription;

    @Column(name="data_map_adquirente")
    private String mapAdquiriente;

    @Column(name="data_map_amount")
    private String mapAmount;

    @Column(name="data_map_transaction_id")
    private String mapTransactionId;

    @Column(name="aditional_information")
    private String aditionalInformation;

    @Column(name="creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name="data_map_proccess_code")
    private String mapProcessCode;

    @Column(name="qr_tag_id")
    private String qrTagId;

    @Column(name="visanet_callback_request")
    private String visanetCallbackRequest;

    @Column(name="id_bitacora")
    private Long idBitacora;

    public VisanetTransactionCallback() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Boolean getSuccessful() {
        return successful;
    }

    public void setSuccessful(Boolean successful) {
        this.successful = successful;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMapIdUnico() {
        return mapIdUnico;
    }

    public void setMapIdUnico(String mapIdUnico) {
        this.mapIdUnico = mapIdUnico;
    }

    public String getMapAuthorizationCode() {
        return mapAuthorizationCode;
    }

    public void setMapAuthorizationCode(String mapAuthorizationCode) {
        this.mapAuthorizationCode = mapAuthorizationCode;
    }

    public String getMapCard() {
        return mapCard;
    }

    public void setMapCard(String mapCard) {
        this.mapCard = mapCard;
    }

    public String getMapTerminal() {
        return mapTerminal;
    }

    public void setMapTerminal(String mapTerminal) {
        this.mapTerminal = mapTerminal;
    }

    public String getMapTransactionDate() {
        return mapTransactionDate;
    }

    public void setMapTransactionDate(String mapTransactionDate) {
        this.mapTransactionDate = mapTransactionDate;
    }

    public String getMapActionCode() {
        return mapActionCode;
    }

    public void setMapActionCode(String mapActionCode) {
        this.mapActionCode = mapActionCode;
    }

    public String getMapStatus() {
        return mapStatus;
    }

    public void setMapStatus(String mapStatus) {
        this.mapStatus = mapStatus;
    }

    public String getMapActionDescription() {
        return mapActionDescription;
    }

    public void setMapActionDescription(String mapActionDescription) {
        this.mapActionDescription = mapActionDescription;
    }

    public String getMapAdquiriente() {
        return mapAdquiriente;
    }

    public void setMapAdquiriente(String mapAdquiriente) {
        this.mapAdquiriente = mapAdquiriente;
    }

    public String getMapAmount() {
        return mapAmount;
    }

    public void setMapAmount(String mapAmount) {
        this.mapAmount = mapAmount;
    }

    public String getMapTransactionId() {
        return mapTransactionId;
    }

    public void setMapTransactionId(String mapTransactionId) {
        this.mapTransactionId = mapTransactionId;
    }

    public String getAditionalInformation() {
        return aditionalInformation;
    }

    public void setAditionalInformation(String aditionalInformation) {
        this.aditionalInformation = aditionalInformation;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getMapProcessCode() {
        return mapProcessCode;
    }

    public void setMapProcessCode(String mapProcessCode) {
        this.mapProcessCode = mapProcessCode;
    }

    public String getQrTagId() {
        return qrTagId;
    }

    public void setQrTagId(String qrTagId) {
        this.qrTagId = qrTagId;
    }

    public String getVisanetCallbackRequest() {
        return visanetCallbackRequest;
    }

    public void setVisanetCallbackRequest(String visanetCallbackRequest) {
        this.visanetCallbackRequest = visanetCallbackRequest;
    }


    public Long getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(Long idBitacora) {
        this.idBitacora = idBitacora;
    }
}
