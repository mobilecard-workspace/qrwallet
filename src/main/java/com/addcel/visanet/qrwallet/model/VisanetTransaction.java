package com.addcel.visanet.qrwallet.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "visanet_transaction")
@Access(AccessType.FIELD)
public class VisanetTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_user")
    private Long idUser;

    @Column(name = "id_application")
    private Long idApplication;

    @Column(name = "id_country")
    private Long idCountry;

    @Column(name = "language")
    private String language;

    @Column(name = "transaction_type")
    private Long transactionType;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "comision")
    private BigDecimal comision;

    @Column(name = "description")
    private String description;

    @Column(name = "id_bitacora")
    private Long idBitacora;

    @Column(name = "id_lcpf_establecimiento")
    private Long idLcpfEstablecimiento;

    @Column(name = "status")
    private Long status;

    @Column(name = "credit_card_id")
    private Long cardId;

    @Column(name = "credit_card_number")
    private String cardNumber;

    @Column(name = "credit_card_expiration")
    private String cardExpirationDate;

    @Column(name = "credit_card_cvv")
    private String cardCvv;

    @Column(name = "credit_card_holder_name")
    private String cardHolderFirstName;

    @Column(name = "credit_card_holder_last_name")
    private String cardHolderLastName;

    @Column(name = "qr_expiration_date")
    @Temporal(TemporalType.DATE)
    private Date qrExpirationDate;

    @Column(name = "generate_qr_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date generateQrDate;

    @Column(name = "generate_qr_code")
    private Long generateQrCode;

    @Column(name = "generate_qr_message")
    private String generateQrMessage;

    @Column(name = "auth_card_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date authCardDate;

    @Column(name = "auth_card_code")
    private Long authCardCode;

    @Column(name = "auth_card_message")
    private String authCardMessage;

    @Column(name = "qr_tag_id")
    private String qrTagId;

    @Column(name = "qr_md5")
    private String qrMd5;

    @Column(name = "qr_image_base64")
    private String qrImageBase64;

    @Column(name = "ecore_transaction_uuid")
    private String ecoreTransactionUuid;

    @Column(name = "ecore_transaction_date")
    private String ecoreTransactionDate;

    @Column(name = "ecore_millis")
    private Long ecoreMillis;

    @Column(name = "order_purchase_number")
    private String orderPurchaseNumber;

    @Column(name = "order_amount")
    private String orderAmount;

    @Column(name = "order_installment")
    private String orderInstallment;

    @Column(name = "order_currency")
    private String orderCurrency;

    @Column(name = "order_external_transaction_id")
    private String orderExternalTransactionId;

    @Column(name = "order_authorized_amount")
    private String orderAuthorizedAmount;

    @Column(name = "order_authorization_code")
    private String orderAuthorizationCode;

    @Column(name = "order_action_code")
    private String orderActionCode;

    @Column(name = "order_trace_number")
    private String orderTraceNumber;

    @Column(name = "order_transaction_date")
    private String orderTransactionDate;

    @Column(name = "order_transaction_id")
    private String orderTransactionId;

    @Column(name = "map_idunico")
    private String mapIdUnico;

    @Column(name = "map_merchant")
    private String mapMerchant;

    @Column(name = "map_authorizationcode")
    private String mapAuthorizationCode;

    @Column(name = "map_card")
    private String mapCard;

    @Column(name = "map_currency")
    private String mapCurrency;

    @Column(name = "map_terminal")
    private String mapTerminal;

    @Column(name = "map_transactiondate")
    private String mapTransactionDate;

    @Column(name = "map_actioncode")
    private String mapActionCode;

    @Column(name = "map_status")
    private String mapStatus;

    @Column(name = "map_action_description")
    private String mapActionDescription;

    @Column(name = "map_adquirente")
    private String mapAdquiriente;

    @Column(name = "map_amount")
    private String mapAmount;

    @Column(name = "map_process_code")
    private String mapProcessCode;

    @Column(name = "map_transactionid")
    private String mapTransactionId;

    @Column(name = "map_tracenumber")
    private String mapTraceNumber;


    @Column(name = "business_name")
    private String businessName;

    @Column(name = "email")
    private String email;

    @Column(name = "payment_type")
    private String paymentType;

    @Column(name = "auth_url")
    private String authUrl;

    @Column(name = "service_request")
    private String serviceRequest;

    @Column(name = "service_response")
    private String serviceResponse;

    @Column(name = "visanet_request")
    private String visanetRequest;

    @Column(name = "visanet_response")
    private String visanetResponse;

    @Column(name = "card_type")
    private String cardType;




    public VisanetTransaction() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdApplication() {
        return idApplication;
    }

    public void setIdApplication(Long idApplication) {
        this.idApplication = idApplication;
    }

    public Long getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(Long idCountry) {
        this.idCountry = idCountry;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(Long idBitacora) {
        this.idBitacora = idBitacora;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getQrExpirationDate() {
        return qrExpirationDate;
    }

    public void setQrExpirationDate(Date qrExpirationDate) {
        this.qrExpirationDate = qrExpirationDate;
    }

    public Date getGenerateQrDate() {
        return generateQrDate;
    }

    public void setGenerateQrDate(Date generateQrDate) {
        this.generateQrDate = generateQrDate;
    }

    public Long getGenerateQrCode() {
        return generateQrCode;
    }

    public void setGenerateQrCode(Long generateQrCode) {
        this.generateQrCode = generateQrCode;
    }

    public String getGenerateQrMessage() {
        return generateQrMessage;
    }

    public void setGenerateQrMessage(String generateQrMessage) {
        this.generateQrMessage = generateQrMessage;
    }

    public Date getAuthCardDate() {
        return authCardDate;
    }

    public void setAuthCardDate(Date authCardDate) {
        this.authCardDate = authCardDate;
    }

    public Long getAuthCardCode() {
        return authCardCode;
    }

    public void setAuthCardCode(Long authCardCode) {
        this.authCardCode = authCardCode;
    }

    public String getAuthCardMessage() {
        return authCardMessage;
    }

    public void setAuthCardMessage(String authCardMessage) {
        this.authCardMessage = authCardMessage;
    }

    public String getQrTagId() {
        return qrTagId;
    }

    public void setQrTagId(String qrTagId) {
        this.qrTagId = qrTagId;
    }

    public String getQrMd5() {
        return qrMd5;
    }

    public void setQrMd5(String qrMd5) {
        this.qrMd5 = qrMd5;
    }

    public String getQrImageBase64() {
        return qrImageBase64;
    }

    public void setQrImageBase64(String qrImageBase64) {
        this.qrImageBase64 = qrImageBase64;
    }

    public String getEcoreTransactionUuid() {
        return ecoreTransactionUuid;
    }

    public void setEcoreTransactionUuid(String ecoreTransactionUuid) {
        this.ecoreTransactionUuid = ecoreTransactionUuid;
    }

    public String getEcoreTransactionDate() {
        return ecoreTransactionDate;
    }

    public void setEcoreTransactionDate(String ecoreTransactionDate) {
        this.ecoreTransactionDate = ecoreTransactionDate;
    }

    public Long getEcoreMillis() {
        return ecoreMillis;
    }

    public void setEcoreMillis(Long ecoreMillis) {
        this.ecoreMillis = ecoreMillis;
    }

    public String getOrderPurchaseNumber() {
        return orderPurchaseNumber;
    }

    public void setOrderPurchaseNumber(String orderPurchaseNumber) {
        this.orderPurchaseNumber = orderPurchaseNumber;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderInstallment() {
        return orderInstallment;
    }

    public void setOrderInstallment(String orderInstallment) {
        this.orderInstallment = orderInstallment;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public void setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public String getOrderExternalTransactionId() {
        return orderExternalTransactionId;
    }

    public void setOrderExternalTransactionId(String orderExternalTransactionId) {
        this.orderExternalTransactionId = orderExternalTransactionId;
    }

    public String getOrderAuthorizedAmount() {
        return orderAuthorizedAmount;
    }

    public void setOrderAuthorizedAmount(String orderAuthorizedAmount) {
        this.orderAuthorizedAmount = orderAuthorizedAmount;
    }

    public String getOrderAuthorizationCode() {
        return orderAuthorizationCode;
    }

    public void setOrderAuthorizationCode(String orderAuthorizationCode) {
        this.orderAuthorizationCode = orderAuthorizationCode;
    }

    public String getOrderActionCode() {
        return orderActionCode;
    }

    public void setOrderActionCode(String orderActionCode) {
        this.orderActionCode = orderActionCode;
    }

    public String getOrderTraceNumber() {
        return orderTraceNumber;
    }

    public void setOrderTraceNumber(String orderTraceNumber) {
        this.orderTraceNumber = orderTraceNumber;
    }

    public String getOrderTransactionDate() {
        return orderTransactionDate;
    }

    public void setOrderTransactionDate(String orderTransactionDate) {
        this.orderTransactionDate = orderTransactionDate;
    }

    public String getOrderTransactionId() {
        return orderTransactionId;
    }

    public void setOrderTransactionId(String orderTransactionId) {
        this.orderTransactionId = orderTransactionId;
    }

    public String getMapIdUnico() {
        return mapIdUnico;
    }

    public void setMapIdUnico(String mapIdUnico) {
        this.mapIdUnico = mapIdUnico;
    }

    public String getMapMerchant() {
        return mapMerchant;
    }

    public void setMapMerchant(String mapMerchant) {
        this.mapMerchant = mapMerchant;
    }

    public String getMapAuthorizationCode() {
        return mapAuthorizationCode;
    }

    public void setMapAuthorizationCode(String mapAuthorizationCode) {
        this.mapAuthorizationCode = mapAuthorizationCode;
    }

    public String getMapCard() {
        return mapCard;
    }

    public void setMapCard(String mapCard) {
        this.mapCard = mapCard;
    }

    public String getMapCurrency() {
        return mapCurrency;
    }

    public void setMapCurrency(String mapCurrency) {
        this.mapCurrency = mapCurrency;
    }

    public String getMapTerminal() {
        return mapTerminal;
    }

    public void setMapTerminal(String mapTerminal) {
        this.mapTerminal = mapTerminal;
    }

    public String getMapTransactionDate() {
        return mapTransactionDate;
    }

    public void setMapTransactionDate(String mapTransactionDate) {
        this.mapTransactionDate = mapTransactionDate;
    }

    public String getMapActionCode() {
        return mapActionCode;
    }

    public void setMapActionCode(String mapActionCode) {
        this.mapActionCode = mapActionCode;
    }

    public String getMapStatus() {
        return mapStatus;
    }

    public void setMapStatus(String mapStatus) {
        this.mapStatus = mapStatus;
    }

    public String getMapActionDescription() {
        return mapActionDescription;
    }

    public void setMapActionDescription(String mapActionDescription) {
        this.mapActionDescription = mapActionDescription;
    }

    public String getMapAdquiriente() {
        return mapAdquiriente;
    }

    public void setMapAdquiriente(String mapAdquiriente) {
        this.mapAdquiriente = mapAdquiriente;
    }

    public String getMapAmount() {
        return mapAmount;
    }

    public void setMapAmount(String mapAmount) {
        this.mapAmount = mapAmount;
    }

    public String getMapProcessCode() {
        return mapProcessCode;
    }

    public void setMapProcessCode(String mapProcessCode) {
        this.mapProcessCode = mapProcessCode;
    }

    public String getMapTransactionId() {
        return mapTransactionId;
    }

    public void setMapTransactionId(String mapTransactionId) {
        this.mapTransactionId = mapTransactionId;
    }

    public String getMapTraceNumber() {
        return mapTraceNumber;
    }

    public void setMapTraceNumber(String mapTraceNumber) {
        this.mapTraceNumber = mapTraceNumber;
    }


    public Long getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Long transactionType) {
        this.transactionType = transactionType;
    }

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardExpirationDate() {
        return cardExpirationDate;
    }

    public void setCardExpirationDate(String cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
    }

    public String getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(String cardCvv) {
        this.cardCvv = cardCvv;
    }

    public String getCardHolderFirstName() {
        return cardHolderFirstName;
    }

    public void setCardHolderFirstName(String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }

    public String getCardHolderLastName() {
        return cardHolderLastName;
    }

    public void setCardHolderLastName(String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }

    public Long getIdLcpfEstablecimiento() {
        return idLcpfEstablecimiento;
    }

    public void setIdLcpfEstablecimiento(Long idLcpfEstablecimiento) {
        this.idLcpfEstablecimiento = idLcpfEstablecimiento;
    }

    public BigDecimal getComision() {
        return comision;
    }

    public void setComision(BigDecimal comision) {
        this.comision = comision;
    }


    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    public String getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(String serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    public String getServiceResponse() {
        return serviceResponse;
    }

    public void setServiceResponse(String serviceResponse) {
        this.serviceResponse = serviceResponse;
    }

    public String getVisanetRequest() {
        return visanetRequest;
    }

    public void setVisanetRequest(String visanetRequest) {
        this.visanetRequest = visanetRequest;
    }

    public String getVisanetResponse() {
        return visanetResponse;
    }

    public void setVisanetResponse(String visanetResponse) {
        this.visanetResponse = visanetResponse;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
}
