package com.addcel.visanet.qrwallet.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication()
@ComponentScan({"com.addcel.visanet.qrwallet", "com.addcel.visanet.api",})
@EnableJpaRepositories(basePackages={"com.addcel.visanet.qrwallet.repositories", })
@EntityScan({"com.addcel.visanet.qrwallet.model", })
public class VisaQrWalletServiceApplication  extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(VisaQrWalletServiceApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(VisaQrWalletServiceApplication.class);
	}
}
