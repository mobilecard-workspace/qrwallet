package com.addcel.visanet.qrwallet.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.addcel.visanet.qrwallet.model.LcpfEstablecimiento;

@Repository
public interface LcpfEstablecimientoReposiotry extends CrudRepository<LcpfEstablecimiento, Long> {

    
}
