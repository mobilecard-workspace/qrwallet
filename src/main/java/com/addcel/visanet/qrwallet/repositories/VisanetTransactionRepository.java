package com.addcel.visanet.qrwallet.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.addcel.visanet.qrwallet.model.TBitacora;
import com.addcel.visanet.qrwallet.model.VisanetTransaction;

import java.util.List;

@Repository
public interface VisanetTransactionRepository extends CrudRepository<VisanetTransaction, Long> {

    public List<VisanetTransaction> findByQrMd5(String qrMd5);

}
