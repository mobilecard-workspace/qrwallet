package com.addcel.visanet.qrwallet.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.addcel.visanet.qrwallet.model.TarjetaCredito;

@Repository
public interface TarjetaCreditoRepository extends CrudRepository<TarjetaCredito, Long> {

}
