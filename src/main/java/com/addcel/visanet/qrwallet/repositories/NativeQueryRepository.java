package com.addcel.visanet.qrwallet.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.addcel.visanet.qrwallet.model.mappings.LongMappingDTO;


//@Repository
//@NamedNativeQuery(
//        name = "selectIdUsuarioByIdBitacora",
//        query = "SELECT id_usuario FROM mobilecard.t_bitacora WHERE id_bitacora=:idBitacora",
//        resultClass = com.addcel.visanet.qrwallet.model.mappings.LongMappingDTO.class,
//        resultSetMapping = "mapIdUsuario"
//        
//        )
@Repository
public class NativeQueryRepository {

    @PersistenceContext
    private EntityManager em;
    
    @SuppressWarnings("unchecked")
    public Long  obtener (Long id) {
        List<Object> o = em.createNativeQuery("SELECT id_usuario FROM mobilecard.t_bitacora WHERE id_bitacora=:idBitacora", "mapIdUsuario").setParameter("idBitacora", id).getResultList();
        LongMappingDTO result = o == null || o.isEmpty() ? null : (LongMappingDTO)o.get(0);
        return result == null ? null : result.getValue();
    }
    
}
