package com.addcel.visanet.qrwallet.repositories;

import com.addcel.visanet.qrwallet.model.VisanetGeneratedQrTransaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VisanetGeneratedQrTransactionRepository extends CrudRepository<VisanetGeneratedQrTransaction, Long> {

    public List<VisanetGeneratedQrTransaction> findByQrId(String qrId);

    public List<VisanetGeneratedQrTransaction> findByQrIdAndStatus(String qrId, Integer status);
}
