package com.addcel.visanet.qrwallet.services;

import com.addcel.visanet.api.dto.GenerateQrParam;
import com.addcel.visanet.api.dto.GenerateQrRequest;
import com.addcel.visanet.api.dto.GenerateQrResponse;
import com.addcel.visanet.qrwallet.dto.CreatePaymentQrRequest;
import com.addcel.visanet.qrwallet.dto.CreatePaymentQrResponse;
import com.addcel.visanet.qrwallet.model.VisanetGeneratedQrTransaction;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@RestController
public class VisaNetGenerateQrService extends BaseQrWalletService {

    private static final Logger LOGGER = LogManager.getLogger(VisaNetGenerateQrService.class);

    public VisaNetGenerateQrService() {
        super();
    }

    //generar qr comercio
    @RequestMapping(value = "{idApp}/{idCountry}/{language}/{businessId}/business/generate/qr", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CreatePaymentQrResponse> generateQrBusiness(@PathVariable("idApp") Long applicationId, @PathVariable("idCountry") Long countryId, @PathVariable("language") String language, @PathVariable("businessId") Long businessId, @RequestBody CreatePaymentQrRequest request) throws Exception {
        LOGGER.info("VisaQrWalletService - generateQrBusiness init - applicationId=" + applicationId + ", countryId=" + countryId + ", language=" + language + ", businessId" + businessId + ", request=" + request);

        BigDecimal minAmount;
        try {
            minAmount = new BigDecimal(env.getProperty("visanet.qr.min.amount")).setScale(2, RoundingMode.HALF_EVEN);
        } catch (Exception e) {
            minAmount = BigDecimal.ZERO;
        }

        if (request.getExpirationDate() == null || request.getExpirationDate().compareTo(Calendar.getInstance().getTime()) <= 0) {
            throw new Exception("QR ExpirationDate =" + (request.getExpirationDate() == null ? "null" : request.getExpirationDate().toString()) + " not valid.");
        }

        if (request.getDescription() == null || request.getDescription().isEmpty()) {
            throw new Exception("A transaction description is required");
        }

        BigDecimal amount = request.getAmount() == null ? null : request.getAmount().setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal comision = request.getComision() == null ? null : request.getComision().setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal totalAmount = (amount == null ? BigDecimal.ZERO : amount).add(comision == null ? BigDecimal.ZERO : comision).setScale(2, RoundingMode.HALF_EVEN);

        // validar monto mínimo si el monto viene
        if (amount != null && minAmount.compareTo(BigDecimal.ZERO) > 0 && amount.compareTo(minAmount) < 0) {
            throw new Exception("Invalid transaction amount=" + amount.toString() + ", minimum transaction amount=" + minAmount);
        }

        // se crea transaccion con monto nulo
        VisanetGeneratedQrTransaction tx = new VisanetGeneratedQrTransaction();
        tx.setApplicationId(applicationId);
        tx.setCountryId(countryId);
        tx.setLanguageId(language);
        tx.setAmount(amount);
        tx.setComision(comision);
        tx.setDescription(request.getDescription());
        tx.setExpirationDate(request.getExpirationDate());
        tx.setBusinessId(businessId > 0 ? businessId : null);
        tx.setUserId(null);
        tx.setCreationDate(null);
        tx.setGeneratedQr(false);
        tx.setExpirationDate(request.getExpirationDate());

        //login en API
        String token;
        try {
            token = apiLogin.generateLoginToken();
            if (token == null) {
                tx.setStatus(2001);
                visanetGeneratedQrRepository.save(tx);
                throw new Exception("Unable to generate API Security Token.");
            }
        } catch (Exception e) {
            tx.setStatus(2001);
            visanetGeneratedQrRepository.save(tx);
            LOGGER.info("VisaQrWalletService - generateQrBusiness - Exception generating security token " + e);
            throw new Exception("Error in consuming auth service in visanet gateway. Server error: " + e.getMessage());
        }
        LOGGER.info("VisaQrWalletService - generateQr - Visanet auth token created");


        //generar Qr
        boolean amountSet = amount != null && amount.compareTo(BigDecimal.ZERO) > 0;
        DateFormat format = new SimpleDateFormat(env.getProperty("visanet.api.generateqr.expirationdateformat").trim());
        String qrType = amountSet ? env.getProperty("visanet.qr.type.dynamic") : env.getProperty("visanet.qr.type.static");
        GenerateQrRequest genQrRequest = new GenerateQrRequest(true, qrType, format.format(request.getExpirationDate()));

        List<GenerateQrParam> param = genQrRequest.getParam();
        param.add(new GenerateQrParam("merchantId", env.getProperty("visanet.api.merchantid").trim()));
        param.add(new GenerateQrParam("transactionCurrency", env.getProperty("visanet.api.currency.pen.code").trim()));

        if (amountSet) {
            LOGGER.info("VisaQrWalletService - generateQrBusiness - amountSet = true, se setea el monto");
            param.add(new GenerateQrParam("transactionAmount", amount.toString()));
        } else {
            LOGGER.info("VisaQrWalletService - generateQrBusiness - amountSet = false, NO se setea el monto");
        }

        GenerateQrResponse genQrResponse;
        try {
            genQrResponse = apiLogin.generateQr(token, genQrRequest);
            LOGGER.info("VisaQrWalletService - generateQrBusiness - genQrResponse = " + genQrResponse);
        } catch (Exception e) {
            tx.setStatus(2002);
            visanetGeneratedQrRepository.save(tx);
            LOGGER.info("VisaQrWalletService - generateQrBusiness - Exception consuming qr service " + e);
            throw new Exception("Error in consuming qr service in visanet gateway. Server error: " + e.getMessage());
        }
        LOGGER.info("VisaQrWalletService - generateQrBusiness - Visanet qr service returned code=" + (genQrResponse == null ? "null" : genQrResponse.getCodeResponse()));

        //se guarda code y message devuelto por el servicio en la transacción
        Calendar creationTime = Calendar.getInstance();
        tx.setVisanetQrResponse(new ObjectMapper().writeValueAsString(genQrResponse));
        if (genQrResponse == null || genQrResponse.getCodeResponse() != 0L) {
            tx.setStatus(2003);
            visanetGeneratedQrRepository.save(tx);
            throw new Exception("Error consuming Visa Integration API Generate QR code=" + genQrResponse.getCodeResponse() + ", message= " + genQrResponse.getMessage());
        }

        //imagen en base 64
        String tagImg = genQrResponse.getTagImg();
        int n = tagImg.indexOf(",");
        String imageBase64 = (tagImg != null && n > 0) ? tagImg.substring(n + 1) : "";
        LOGGER.info("VisaQrWalletService - generateQrBusiness - imageBase64 =" + imageBase64);

        tx.setStatus(2000);
        tx.setGeneratedQr(true);
        tx.setQrId(genQrResponse.getTagId());
        tx.setCreationDate(creationTime.getTime());
        LOGGER.info("VisaQrWalletService - generateQrBusiness - transaction saved in visanet_transaction table");
        visanetGeneratedQrRepository.save(tx);

        LOGGER.info("VisaQrWalletService - generateQrBusiness - service return OK");
        return new ResponseEntity<>(new CreatePaymentQrResponse(0L, "successful", genQrResponse.getTagId(), imageBase64), HttpStatus.OK);
    }

    // generar qr de pago con id de usuario y/o id de negocio
    @RequestMapping(value = "{idApp}/{idCountry}/{language}/{userId}/{businessId}/payment/generate/qr", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CreatePaymentQrResponse> generateQrBusinessUser(@PathVariable("idApp") Long applicationId, @PathVariable("idCountry") Long countryId, @PathVariable("language") String language, @PathVariable("userId") Long userId, @PathVariable("businessId") Long businessId, @RequestBody CreatePaymentQrRequest request) throws Exception {
        LOGGER.info("VisaQrWalletService - generateQrBusinessUser init - applicationId=" + applicationId + ", countryId=" + countryId + ", language=" + language + ", userId" + userId + ", businessId" + businessId + "request=" + request);

        BigDecimal minAmount;
        try {
            minAmount = new BigDecimal(env.getProperty("visanet.qr.min.amount")).setScale(2, RoundingMode.HALF_EVEN);
        } catch (Exception e) {
            minAmount = BigDecimal.ZERO;
        }

        if (request.getAmount() == null || request.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            throw new Exception("Transaction Amount  must be greater than zero");
        }

        if (minAmount.compareTo(BigDecimal.ZERO) > 0 && request.getAmount().compareTo(minAmount) < 0) {
            throw new Exception("Invalid transaction amount=" + request.getAmount().toString() + ", minimum transaction amount=" + minAmount);
        }

        if (request.getExpirationDate() == null || request.getExpirationDate().compareTo(Calendar.getInstance().getTime()) <= 0) {
            throw new Exception("QR ExpirationDate =" + (request.getExpirationDate() == null ? "null" : request.getExpirationDate().toString()) + " not valid.");
        }

        if (request.getDescription() == null || request.getDescription().isEmpty()) {
            throw new Exception("A transaction description is required");
        }

        Boolean userIdExistsInBd = userId != null && usuarioRepository.existsById(userId);


        //crear la transacción
        BigDecimal comision = request.getComision() == null ? BigDecimal.ZERO : request.getComision();


        VisanetGeneratedQrTransaction tx = new VisanetGeneratedQrTransaction();
        tx.setApplicationId(applicationId);
        tx.setCountryId(countryId);
        tx.setLanguageId(language);
        tx.setAmount(request.getAmount());
        tx.setComision(request.getComision());
        tx.setDescription(request.getDescription() + (comision != null ? ", comision=" + comision.toString() : ""));
        tx.setExpirationDate(request.getExpirationDate());
        tx.setBusinessId(businessId > 0L ? businessId : null);
        tx.setUserId(userId > 0L ? userId : null);
        tx.setGeneratedQr(false);
        tx.setCreationDate(null);


        //login en API
        String token;
        try {
            token = apiLogin.generateLoginToken();
            if (token == null) {
                tx.setStatus(3001);
                visanetGeneratedQrRepository.save(tx);
                throw new Exception("Unable to generate API Security Token.");
            }
        } catch (Exception e) {
            tx.setStatus(3001);
            visanetGeneratedQrRepository.save(tx);
            LOGGER.info("VisaQrWalletService - generateQrBusinessUser - Exception generating security token " + e);
            throw new Exception("Error in consuming auth service in visanet gateway. Server error: " + e.getMessage());
        }
        LOGGER.info("VisaQrWalletService - generateQrBusinessUser - Visanet auth token created");


        //generar Qr
        DateFormat format = new SimpleDateFormat(env.getProperty("visanet.api.generateqr.expirationdateformat").trim());
        GenerateQrRequest genQrRequest = new GenerateQrRequest(true, env.getProperty("visanet.qr.type.dynamic"), format.format(request.getExpirationDate()));

        List<GenerateQrParam> param = genQrRequest.getParam();
        param.add(new GenerateQrParam("merchantId", env.getProperty("visanet.api.merchantid").trim()));
        param.add(new GenerateQrParam("transactionCurrency", env.getProperty("visanet.api.currency.pen.code").trim()));
        param.add(new GenerateQrParam("transactionAmount", request.getAmount().setScale(2, RoundingMode.HALF_EVEN).toString()));

        GenerateQrResponse genQrResponse;
        try {
            genQrResponse = apiLogin.generateQr(token, genQrRequest);
        } catch (Exception e) {
            tx.setStatus(3002);
            visanetGeneratedQrRepository.save(tx);
            LOGGER.info("VisaQrWalletService - generateQrBusinessUser - Exception consuming qr service " + e);
            throw new Exception("Error in consuming qr service in visanet gateway. Server error: " + e.getMessage());
        }
        LOGGER.info("VisaQrWalletService - generateQr - Visanet qr service returned code=" + (genQrResponse == null ? "null" : genQrResponse.getCodeResponse()));

        //se guarda code y message devuelto por el servicio en la transacción
        tx.setVisanetQrResponse(new ObjectMapper().writeValueAsString(genQrResponse));
        Calendar creationTime = Calendar.getInstance();

        if (genQrResponse == null || genQrResponse.getCodeResponse() != 0L) {
            tx.setStatus(3002);
            visanetGeneratedQrRepository.save(tx);
            throw new Exception("Error consuming Visa Integration API Generate QR code=" + genQrResponse.getCodeResponse() + ", message= " + genQrResponse.getMessage());
        }

        //imagen en base 64
        String tagImg = genQrResponse.getTagImg();
        int n = tagImg.indexOf(",");
        String imageBase64 = (tagImg != null && n > 0) ? tagImg.substring(n + 1) : "";

        tx.setStatus(3000);
        tx.setGeneratedQr(true);
        tx.setQrId(genQrResponse.getTagId());
        tx.setCreationDate(creationTime.getTime());
        LOGGER.info("VisaQrWalletService - generateQrBusinessUser - transaction saved in visanet_transaction table");
        visanetGeneratedQrRepository.save(tx);

        LOGGER.info("VisaQrWalletService - generateQrBusinessUser - service return OK");
        return new ResponseEntity<>(new CreatePaymentQrResponse(0L, "successful", genQrResponse.getTagId(), imageBase64), HttpStatus.OK);
    }

    @RequestMapping(value = "{idApp}/{idCountry}/{language}/{businessId}/generate/qr", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CreatePaymentQrResponse> generateQr(@PathVariable("idApp") Long applicationId, @PathVariable("idCountry") Long countryId, @PathVariable("language") String language, @PathVariable("businessId") Long businessId, @RequestBody CreatePaymentQrRequest request) throws Exception {
        LOGGER.info("VisaQrWalletService - generateQr init - applicationId=" + applicationId + ", countryId=" + countryId + ", language=" + language + ", businessId" + businessId + "request=" + request);



        BigDecimal minAmount;
        try {
            minAmount = new BigDecimal(env.getProperty("visanet.qr.min.amount")).setScale(2, RoundingMode.HALF_EVEN);
        } catch (Exception e) {
            minAmount = BigDecimal.ZERO;
        }

        boolean hasAmount = request.getAmount() != null;


        if (hasAmount  && request.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            throw new Exception("Transaction Amount  must be greater than zero.");
        }

        if (hasAmount && minAmount.compareTo(BigDecimal.ZERO) > 0 && request.getAmount().compareTo(minAmount) < 0) {
            throw new Exception("Invalid transaction amount=" + request.getAmount().toString() + ", minimum transaction amount=" + minAmount);
        }

        if (request.getExpirationDate() == null || request.getExpirationDate().compareTo(Calendar.getInstance().getTime()) <= 0) {
            throw new Exception("QR ExpirationDate =" + (request.getExpirationDate() == null ? "null" : request.getExpirationDate().toString()) + " not valid.");
        }

        if (request.getDescription() == null || request.getDescription().isEmpty()) {
            throw new Exception("A transaction description is required");
        }

        //crear la transacción
        BigDecimal comision = request.getComision() == null ? BigDecimal.ZERO : request.getComision();

        VisanetGeneratedQrTransaction tx = new VisanetGeneratedQrTransaction();
        tx.setApplicationId(applicationId);
        tx.setCountryId(countryId);
        tx.setLanguageId(language);
        tx.setAmount(request.getAmount());
        tx.setComision(comision);
        tx.setDescription(request.getDescription() + (comision != null ? ", comision=" + comision.toString() : ""));
        tx.setExpirationDate(request.getExpirationDate());
        tx.setBusinessId(businessId);
        tx.setUserId(null);
        tx.setCreationDate(null);
        tx.setGeneratedQr(false);


        //login en API
        String token;
        try {
            token = apiLogin.generateLoginToken();
            if (token == null) {
                tx.setStatus(4001);
                visanetGeneratedQrRepository.save(tx);
                throw new Exception("Unable to generate API Security Token.");
            }
        } catch (Exception e) {
            tx.setStatus(4001);
            visanetGeneratedQrRepository.save(tx);
            LOGGER.info("VisaQrWalletService - generateQr - Exception generating security token " + e);
            throw new Exception("Error in consuming auth service in visanet gateway. Server error: " + e.getMessage());
        }
        LOGGER.info("VisaQrWalletService - generateQr - Visanet auth token created");


        //generar Qr
        DateFormat format = new SimpleDateFormat(env.getProperty("visanet.api.generateqr.expirationdateformat").trim());

        String qrType = hasAmount ? env.getProperty("visanet.qr.type.dynamic") : env.getProperty("visanet.qr.type.static");

        GenerateQrRequest genQrRequest = new GenerateQrRequest(true, qrType, format.format(request.getExpirationDate()));

        List<GenerateQrParam> param = genQrRequest.getParam();
        param.add(new GenerateQrParam("merchantId", env.getProperty("visanet.api.merchantid").trim()));
        param.add(new GenerateQrParam("transactionCurrency", env.getProperty("visanet.api.currency.pen.code").trim()));
        if (hasAmount) {
            param.add(new GenerateQrParam("transactionAmount", request.getAmount().setScale(2, RoundingMode.HALF_EVEN).toString()));
        }

        GenerateQrResponse genQrResponse;
        try {
            genQrResponse = apiLogin.generateQr(token, genQrRequest);
            LOGGER.info("VisaQrWalletService - generateQr - genQrResponse =" + genQrResponse);
        } catch (Exception e) {
            tx.setStatus(4002);
            visanetGeneratedQrRepository.save(tx);
            LOGGER.info("VisaQrWalletService - generateQr - Exception consuming qr service " + e);
            throw new Exception("Error in consuming qr service in visanet gateway. Server error: " + e.getMessage());
        }
        LOGGER.info("VisaQrWalletService - generateQr - Visanet qr service returned code=" + (genQrResponse == null ? "null" : genQrResponse.getCodeResponse()));

        Calendar creationTime = Calendar.getInstance();
        tx.setVisanetQrResponse(new ObjectMapper().writeValueAsString(genQrResponse));

        if (genQrResponse == null || genQrResponse.getCodeResponse() != 0L) {
            tx.setStatus(4003);
            visanetGeneratedQrRepository.save(tx);
            throw new Exception("Error consuming Visa Integration API Generate QR code=" + genQrResponse.getCodeResponse() + ", message= " + genQrResponse.getMessage());
        }

        //imagen en base 64
        String tagImg = genQrResponse.getTagImg();
        int n = tagImg.indexOf(",");
        String imageBase64 = (tagImg != null && n > 0) ? tagImg.substring(n + 1) : "";

        //generar imagen y extraer data QR
        String qrMd5Checksum = qrReader.calculateQrContentMd5FromBase64Img(imageBase64);
        LOGGER.info("qrMd5Checksum=" + qrMd5Checksum);

        tx.setStatus(4000);
        tx.setGeneratedQr(true);
        tx.setQrId(genQrResponse.getTagId());
        tx.setCreationDate(creationTime.getTime());
        LOGGER.info("VisaQrWalletService - generateQr - transaction saved in visanet_transaction table");

        visanetGeneratedQrRepository.save(tx);

        LOGGER.info("VisaQrWalletService - generateQr - service return OK");
        return new ResponseEntity<>(new CreatePaymentQrResponse(0L, "successful", genQrResponse.getTagId(), imageBase64), HttpStatus.OK);
    }

}
