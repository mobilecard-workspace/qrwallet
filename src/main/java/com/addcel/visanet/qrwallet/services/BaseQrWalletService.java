package com.addcel.visanet.qrwallet.services;

import com.addcel.visanet.api.VisaNetApi;
import com.addcel.visanet.qrwallet.components.CreditCardNumberFormatter;
import com.addcel.visanet.qrwallet.components.QREmvDecoder;
import com.addcel.visanet.qrwallet.components.QRReader;
import com.addcel.visanet.qrwallet.components.StringDecoder;
import com.addcel.visanet.qrwallet.dto.BaseApiResponse;
import com.addcel.visanet.qrwallet.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

public abstract class BaseQrWalletService {

    public BaseQrWalletService() {
        super();
    }

    @Autowired
    protected TarjetaCreditoRepository cardRepository;

    @Autowired
    protected VisanetTransactionRepository visaNetRepository;

    @Autowired
    protected LcpfEstablecimientoReposiotry lcpfEstablecimientoReposiotry;

    @Autowired
    protected UsuarioRepository usuarioRepository;

    @Autowired
    protected QRReader qrReader;

    @Autowired
    protected StringDecoder stringDecoder;

    @Autowired
    protected VisaNetApi apiLogin;

    @Autowired
    protected Environment env;

    @Autowired
    protected TBitacoraRepository tBitacoraRepository;

    @Autowired
    protected VisanetCallbackRepository callbackRepository;

    @Autowired
    protected QREmvDecoder qrEmvDecoder;

    @Autowired
    protected CreditCardNumberFormatter creditCardNumberFormatter;

    @Autowired
    protected VisanetGeneratedQrTransactionRepository visanetGeneratedQrRepository;


    @ExceptionHandler(Exception.class)
    public ResponseEntity<BaseApiResponse> handleException(Exception e) {
        e.printStackTrace();
        Long code = -1L;
        String message = "VisaQrWalletService - callback - handleException - Exception in service: " + e.getMessage();
        return new ResponseEntity<>(new BaseApiResponse(code, message), HttpStatus.OK);

    }

}
