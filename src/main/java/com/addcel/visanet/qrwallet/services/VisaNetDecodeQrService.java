package com.addcel.visanet.qrwallet.services;

import com.addcel.visanet.qrwallet.dto.DecodedQrContentDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.visanet.qrwallet.dto.DecodeQrAsciiRequest;
import com.addcel.visanet.qrwallet.dto.DecodeQrHexaRequest;
import com.addcel.visanet.qrwallet.dto.DecodeQrResponse;

@RestController
public class VisaNetDecodeQrService extends BaseQrWalletService {

    private static final Logger LOGGER = LogManager.getLogger(VisaNetDecodeQrService.class);

    public VisaNetDecodeQrService() {
        super();
    }
    
    @RequestMapping(value = "qr/decode/hexadecimal", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<DecodeQrResponse> decodeQrContentFromHexadecimal(@RequestBody  DecodeQrHexaRequest request) throws Exception {

        LOGGER.info("HOOLA");
        LOGGER.info("VisaQrWalletService.decodeQrContentFromHexadecimal init - hexaCode con lenght=" + (request == null || request.getQrHexa() == null ? 0 : request.getQrHexa().length()));

        //validar
        if (request == null || request.getQrHexa() == null || request.getQrHexa().trim().isEmpty()) {
            System.out.println("VisaQrWalletService.decodeQrContentFromHexadecimal - Exception qr content is null");
            throw new Exception("QR code is null");
        }

        DecodedQrContentDTO qr = qrEmvDecoder.decodeQrCodeHex(request.getQrHexa().trim());

        DecodeQrResponse response  = new DecodeQrResponse(0L, "successful", qr);
        System.out.println("VisaQrWalletService.decodeQrContentFromHexadecimal  - service return OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "qr/decode/ascii", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<DecodeQrResponse> decodeQrContentFromAscii(@RequestBody  DecodeQrAsciiRequest request) throws Exception {
        System.out.println("VisaQrWalletService.decodeQrContentFromAscii init - hexaCode con lenght=" + (request == null || request.getQrAscii() == null ? 0 : request.getQrAscii().length()));

        System.out.println("VisaQrWalletService.decodeQrContentFromAscii init - hexaCode con lenght=" + (request == null || request.getQrAscii() == null ? 0 : request.getQrAscii().length()));

        //validar
        if (request == null || request.getQrAscii() == null || request.getQrAscii().trim().isEmpty()) {
            System.out.println("VisaQrWalletService.decodeQrContentFromAscii - Exception qr content is null");
            throw new Exception("QR code is null");
        }

        DecodedQrContentDTO qr = qrEmvDecoder.decodeQrCodePlain(request.getQrAscii().trim());
        DecodeQrResponse response  = new DecodeQrResponse(0L, "successful", qr);
        System.out.println("VisaQrWalletService.decodeQrContentFromAscii  - service return OK");
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

}
