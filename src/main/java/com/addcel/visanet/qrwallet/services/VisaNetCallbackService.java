package com.addcel.visanet.qrwallet.services;

import com.addcel.visanet.qrwallet.components.PushServiceConsumer;
import com.addcel.visanet.qrwallet.dto.VisaNetCallbackRequest;
import com.addcel.visanet.qrwallet.dto.VisanetCallbackResponse;
import com.addcel.visanet.qrwallet.model.LcpfEstablecimiento;
import com.addcel.visanet.qrwallet.model.TBitacora;
import com.addcel.visanet.qrwallet.model.VisanetGeneratedQrTransaction;
import com.addcel.visanet.qrwallet.model.VisanetTransactionCallback;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
public class VisaNetCallbackService extends BaseQrWalletService {

    private static final Logger LOGGER = LogManager.getLogger(VisaNetCallbackService.class);

    @Autowired
    private PushServiceConsumer pushServiceConsumer;

    public VisaNetCallbackService() {
        super();
    }

    public TBitacora createBitacoraInstance( VisaNetCallbackRequest request, VisanetTransactionCallback tx, LcpfEstablecimiento business, String languageId, Long appId) {
        String bitTicket = "PAGO VISANET CALLBACK CON TARJETA DE CRÉDITO, A NOMBRE DEL COMERCIO: " + (business == null ? "MOBILECARD" : business.getNombreEstablecimiento()) + ", TARJETA #" + request.getMapCard() + ", MONTO S. " + request.getMapAmount();

        TBitacora bitacora = new TBitacora();
        bitacora.setIdUsuario(null);
        bitacora.setIdioma(languageId);
        bitacora.setIdAplicacion(appId);
        bitacora.setIdProveedor(700L);
        bitacora.setIdProducto(7100L);
        Date bitfecha = Calendar.getInstance().getTime();
        bitacora.setFecha(bitfecha);
        bitacora.setHora(bitfecha);
        bitacora.setConcepto("Pago desde callbaack de visanet visanet_transaction_callback.id=" + tx.getId());
        bitacora.setCargo(new BigDecimal(request.getMapAmount()));
        bitacora.setComision(BigDecimal.ZERO);
        bitacora.setCardId(null);
        bitacora.setStatus(1);
        bitacora.setTicket(bitTicket);
        bitacora.setCardId(null);
        bitacora.setTarjetaCompra(request.getMapCard());
        bitacora.setNumeroAutorizacion(request.getMapAuthorizationCode());
        bitacora.setIdEstablecimiento(business == null ? null : business.getId());
        return bitacora;
    }

    public VisanetTransactionCallback createCallbackInstance(VisaNetCallbackRequest request, String visanetRequest) {
        VisanetTransactionCallback tx = new VisanetTransactionCallback();
        tx.setCreationDate(Calendar.getInstance().getTime());
        tx.setSuccessful(request.getSuccessful());
        tx.setAditionalInformation(request.getAditionalInfo());
        tx.setCountryCode(request.getTransactionCountry());
        tx.setCurrencyCode(request.getTransactionCurrency());
        tx.setMapActionCode(request.getMapActionCode());
        tx.setMapActionDescription(request.getMapActionDescription());
        tx.setMapAdquiriente(request.getMapAdquiriente());
        tx.setMapAmount(request.getMapAmount());
        tx.setMapAuthorizationCode(request.getMapAuthorizationCode());
        tx.setMapCard(request.getMapCard());
        tx.setMapIdUnico(request.getMapIdUnico());
        tx.setMapStatus(request.getMapStatus());
        tx.setMapTerminal(request.getMapTerminal());
        tx.setMapTransactionDate(request.getMapTransactionDate());
        tx.setMapTransactionId(request.getMapTransactionId());
        tx.setMerchantId(request.getMerchantId());
        tx.setMerchantName(request.getMerchantName());
        tx.setMapProcessCode(request.getMapProcessCode());
        tx.setQrTagId(request.getQrTagId());
        tx.setVisanetCallbackRequest(visanetRequest);
        return tx;
    }


    // callback
    @RequestMapping(value = "visanet/transaction/callback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<VisanetCallbackResponse> visanetCallback(@RequestBody VisaNetCallbackRequest request) throws Exception {
        LOGGER.info("VisaNetCallbackService - visanetCallback init ");
        String visanetRequest;
        try {
            visanetRequest = new ObjectMapper().writeValueAsString(request);
            LOGGER.info("VisaNetCallbackService - visanetCallback  Cuerpo del Request: ");
            LOGGER.info(visanetRequest);
            LOGGER.info("VisaNetCallbackService - visanetCallback  Fin cuerpo del Request: ");

        } catch (Exception e) {
            visanetRequest = "";
            e.printStackTrace();
        }

        // se crea el registro de transacción desde los datos que vienen en el servicio
        VisanetTransactionCallback tx = createCallbackInstance(request, visanetRequest);
        // tx = callbackRepository.save(tx);

        // sacar la informacion de negocio
        LOGGER.info("VisaNetCallbackService - qrId=" + tx.getQrTagId());
        LOGGER.info("Se busca el qr en la tabla de qrs generados para id=" + tx.getQrTagId());
        List<VisanetGeneratedQrTransaction> qrs = visanetGeneratedQrRepository.findByQrIdAndStatus(request.getQrTagId(), 2000);
        VisanetGeneratedQrTransaction generatedBusinessQr = qrs == null || qrs.isEmpty() ? null : qrs.get(0);

        if (generatedBusinessQr == null) {
            LOGGER.info("VisaNetCallbackService - No existe qr generado para el tagId=" + tx.getQrTagId());
        } else {
            LOGGER.info("VisaNetCallbackService - Existe qr generado con id=" + generatedBusinessQr.getId() + ", businessId" + generatedBusinessQr.getBusinessId());
        }

        Long businessId = generatedBusinessQr == null ? null : generatedBusinessQr.getBusinessId();
        LcpfEstablecimiento business = businessId == null ? null : lcpfEstablecimientoReposiotry.findById(businessId).orElse(null);
        if (business == null) {
            LOGGER.info("VisaNetCallbackService - business=null para tagId=" + tx.getQrTagId());
        } else {
            LOGGER.info("VisaNetCallbackService - businessId=" + business.getId() + ", nombre=" + business.getNombreEstablecimiento() + " para tagId=" + tx.getQrTagId());
        }

        // crear la información en t_bitacora
        LOGGER.info("VisaNetCallbackService - visanetCallback  - generado registro en tabla callback con id = " + tx.getId());
        LOGGER.info("VisaNetCallbackService - visanetCallback  - service return OK");

        Long countryId = generatedBusinessQr == null ? null : generatedBusinessQr.getCountryId();
        String languageId = generatedBusinessQr == null ? null : generatedBusinessQr.getLanguageId();
        Long appId = generatedBusinessQr == null ? null : generatedBusinessQr.getApplicationId();
        String amount = request.getMapAmount();


        TBitacora bitacora = createBitacoraInstance(request, tx, business,  languageId, appId);
        tBitacoraRepository.save(bitacora);

        // actualizar el registro de callback con el id de bitacora
        tx.setIdBitacora(bitacora.getId());
        // callbackRepository.save(tx);

        // enviar el mensaje push
        if (generatedBusinessQr != null && business != null)  {
            LOGGER.info("VisaNetCallbackService - visanetCallback  - envio de mensaj pueh");
            pushServiceConsumer.pushNotification(businessId, countryId, languageId, appId, amount);
        } else {
            LOGGER.info("VisaNetCallbackService - visanetCallback  - No se cargo la infomación del negocio. No se puede hacer el envío del push");
        }

        LOGGER.info("VisaNetCallbackService - visanetCallback  - Fin método, se devuelve estatus exitoso. Creado registro de callback id=" + tx.getId() + ", asociado a id_bitacora=" + bitacora.getId());
        return new ResponseEntity<>(new VisanetCallbackResponse(0L, "successful"), HttpStatus.OK);
    }



}
