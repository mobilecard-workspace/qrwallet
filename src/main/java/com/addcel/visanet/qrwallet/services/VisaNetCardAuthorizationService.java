package com.addcel.visanet.qrwallet.services;

import com.addcel.visanet.api.dto.*;
import com.addcel.visanet.crypto.Crypto;
import com.addcel.visanet.qrwallet.dto.*;
import com.addcel.visanet.qrwallet.model.TBitacora;
import com.addcel.visanet.qrwallet.model.TarjetaCredito;
import com.addcel.visanet.qrwallet.model.VisanetGeneratedQrTransaction;
import com.addcel.visanet.qrwallet.model.VisanetTransaction;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class VisaNetCardAuthorizationService extends BaseQrWalletService {

    private static final Logger LOGGER = LogManager.getLogger(VisaNetCardAuthorizationService.class);

    public VisaNetCardAuthorizationService() {
        super();
    }


    @RequestMapping(value = "{idApp}/{idCountry}/{language}/{userId}/card/authorization", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CardAuthFromQrResponse> authorizePaymentQrCardFromDB(@PathVariable("idApp") Long applicationId,
                                                                               @PathVariable("idCountry") Long countryId, @PathVariable("language") String language, @PathVariable("userId") Long userId,
                                                                               @RequestBody CardAuthFromQrRequest request) throws Exception {
        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB init - applicationId=" + applicationId + ", countryId=" + countryId
                + ", language=" + language + ", userId=" + userId + ", baseAmount=" + request.getBaseAmount() + ", comision=" + request.getComision());

        // validación
        if (userId == null || !usuarioRepository.existsById(userId)) {
            throw new Exception("Record in t_usuarios with id=" + userId + " not found");
        }

        if (request.getQrImageBase64() == null || request.getQrImageBase64().trim().isEmpty()) {
            throw new Exception("An hexadecimal string scanned from QR image is required");
        }

        if (request.getCardHolderFirstName() == null || request.getCardHolderFirstName().trim().isEmpty()) {
            throw new Exception("A Card holder First Name is required");
        }

        if (request.getCardHolderLastName() == null || request.getCardHolderLastName().trim().isEmpty()) {
            throw new Exception("A Card holder Last Name is required.");
        }

        // validar tarjeta de crédito exista en BD y que sea del usuario pasado en la URL
        TarjetaCredito tdc;
        try {
            tdc = cardRepository.findById(request.getCardId()).get();
        } catch (NoSuchElementException e) {
            throw new Exception("Credit card with id=" + request.getCardId() + " not found");
        }

        if (!userId.equals(tdc.getIdUsuario())) {
            throw new Exception("Credit card with id=" + request.getCardId() + " does not belong to user with Id=" + userId);
        }

        DecodedQrContentDTO decodedQr = qrEmvDecoder.decodeQrCodeHex(request.getQrImageBase64());

        //si el tipo de qr es estático se debe validar el monto, en caso contrario se toma del qr
        if (decodedQr.getType() == null || (!decodedQr.getType().equals(env.getProperty("visanet.qr.type.static")) && !decodedQr.getType().equals(env.getProperty("visanet.qr.type.dynamic")))) {
            throw new Exception("Invalid QR Type =" + decodedQr.getType());
        }

        //validar que el merchantId esté en el QR
        if (decodedQr.getMerchantId() == null && decodedQr.getMerchantId().trim().isEmpty()) {
            throw new Exception("Invalid QR merchantId not set");
        }

        // si el qr es estático el monto debe venir el request
        boolean isDynamicQr;
        if (decodedQr.getType().equals(env.getProperty("visanet.qr.type.static"))) {
            if (request.getBaseAmount() == null || request.getBaseAmount().compareTo(BigDecimal.ZERO) <= 0) {
                throw new Exception("For STATIC QR, Transaction Amount  must be set in request greater than zero.");
            }
            isDynamicQr = false;
        } else {
            if (decodedQr.getAmount() == null || decodedQr.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
                throw new Exception("For DYNAMIC QR, Transaction Amount  must be set in QR and be greater than zero.");
            }
            isDynamicQr = true;
        }

        // url base para guardar en BD
        String baseUrl = String.format("%d/%d/%s/%d/card/authorization", applicationId, countryId, language, userId);
        String paymentType = "cardDatabase";

        // monto total
        BigDecimal baseAmount = (isDynamicQr ? decodedQr.getAmount() : request.getBaseAmount()).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal comision;
        if (isDynamicQr) {
            comision = BigDecimal.ZERO;
        } else {
            comision = request.getComision() == null ? BigDecimal.ZERO : request.getComision().setScale(2, RoundingMode.HALF_EVEN);
        }

        final ObjectMapper objectMapper = new ObjectMapper();

        BigDecimal totalAmount = baseAmount.add(comision).setScale(2, RoundingMode.HALF_EVEN);

        // login en API
        String token;
        try {
            token = apiLogin.generateLoginToken();
            if (token == null) {
                throw new Exception("Unable to generate API Security Token.");
            }
        } catch (Exception e) {
            LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - Exception generating security token " + e);
            throw new Exception("Error in consuming auth service in visanet gateway. Server error: " + e.getMessage());
        }
        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - Visanet auth token created");

        // sacar datos de la TDC desde el cifrado
        String tdcNumber = Crypto.aesDecrypt(tdc.getNumero());
        String tdcCvv = Crypto.aesDecrypt(tdc.getCt());
        String tdcExpire = Crypto.aesDecrypt(tdc.getVigencia());
        String maskedCard = creditCardNumberFormatter.maskedCardNumber(tdcNumber);

        String[] expireFields = tdcExpire.split("/");
        String expireMonth = expireFields[0];
        String expireYear = expireFields[1];

        if (expireMonth.charAt(0) == '0') {
            expireMonth = expireMonth.substring(1);
        }
        if (expireYear.length() > 2) {
            expireYear = expireYear.substring(expireYear.length() - 2);
        }

        // tratar de buscar el id de establecimiento
        Long businessId;
        try {
            List<VisanetGeneratedQrTransaction> qrs = visanetGeneratedQrRepository.findByQrIdAndStatus(decodedQr.getId(), 2000);
            VisanetGeneratedQrTransaction generatedBusinessQr = qrs == null || qrs.isEmpty() ? null : qrs.get(0);
            // se prueba con el tag eliminando los 4 primeros caracteres para el caso en que en l Id venga el encabezado del qremv
            if (generatedBusinessQr == null) {
                qrs = visanetGeneratedQrRepository.findByQrIdAndStatus(decodedQr.getId().substring(4), 2000);
                generatedBusinessQr = qrs == null || qrs.isEmpty() ? null : qrs.get(0);
            }

            if (generatedBusinessQr == null) {
                businessId = null;
                LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - No existe qr generado para el tagId=" + decodedQr.getId().substring(3));
            } else {
                businessId = generatedBusinessQr.getBusinessId();
                LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - Existe qr generado con id=" + generatedBusinessQr.getId() + ", businessId" + generatedBusinessQr.getBusinessId());
            }
        } catch (Exception e) {
            businessId = null;
            LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - ocurrió un error tratando de hallar el Id del establecimiento " + e);
            e.printStackTrace();
        }

        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - Creating new transaction in visanet_transaction table");

        String description = request.getDescription() == null || request.getDescription().trim().isEmpty() ? "Transacción TDC En BD idTDC=" + request.getCardId() + ", idUsuario=" + userId : request.getDescription().trim();

        VisanetTransaction tx = new VisanetTransaction();
        tx.setIdApplication(applicationId);
        tx.setIdCountry(countryId);
        tx.setLanguage(language);
        tx.setAmount(totalAmount);
        tx.setComision(comision);
        tx.setDescription(description);
        tx.setQrExpirationDate(null);
        tx.setIdLcpfEstablecimiento(businessId);
        tx.setIdUser(userId);
        tx.setTransactionType(21L);
        tx.setPaymentType(paymentType);
        tx.setAuthUrl(baseUrl);
        tx.setServiceRequest(Crypto.aesEncrypt(objectMapper.writeValueAsString(request)));
        tx.setEmail(request.getEmail() == null || request.getEmail().trim().isEmpty() ? env.getProperty("visanet.default.email") : request.getEmail());
        tx.setCardType(request.getCardType());
        visaNetRepository.save(tx);
        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - Transaction created with id=" + tx.getId());

        // sacar el nombre del comercio a partir de la decodificación del QR.
        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - Trying to decode business information from QR Code");
        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - DecodeQr result= " + (decodedQr == null ? "null" : decodedQr.toString()));
        String businessName = decodedQr.getBusinessName() == null ? "" : decodedQr.getBusinessName().trim();

        // sacar el merchantId y ver si es igual al de mobilecard

        // crear bitacora
        String bitTicket;
        String merchantId = decodedQr.getMerchantId();
               bitTicket = "PAGO VISANET CON TARJETA DE CRÉDITO, A NOMBRE DEL COMERCIO: " + businessName.toUpperCase() + ", TARJETA #" + maskedCard + ", MONTO S. " + totalAmount.toPlainString();

        TBitacora bitacora = new TBitacora();
        bitacora.setIdUsuario(tdc == null ? null : tdc.getIdUsuario());
        bitacora.setIdioma(language);
        bitacora.setIdAplicacion(applicationId);
        bitacora.setIdProveedor(700L);
        bitacora.setIdProducto(7100L);
        Date bitfecha = Calendar.getInstance().getTime();
        bitacora.setFecha(bitfecha);
        bitacora.setHora(bitfecha);
        bitacora.setConcepto(tx.getDescription());
        bitacora.setCargo(baseAmount);
        bitacora.setComision(comision);
        bitacora.setCardId(request.getCardId().intValue());
        bitacora.setStatus(1);
        bitacora.setTicket(bitTicket);
        bitacora.setCardId(request.getCardId().intValue());
        bitacora.setTarjetaCompra(tdc.getNumero());
        tBitacoraRepository.save(bitacora);

        NumberFormat nf = new DecimalFormat("000000000000000000000000000000000000");
        CardAutorizationRequest authRequest = new CardAutorizationRequest();
        authRequest.setCardNumber(tdcNumber);
        authRequest.setCvv2Code("0");
        authRequest.setCreateAlias(Boolean.FALSE.toString());
        authRequest.setAmount(totalAmount.setScale(2, RoundingMode.HALF_EVEN).toString());
        authRequest.setCurrencyId(decodedQr.getCurrencyCode());
        authRequest.setEmail(request.getEmail() == null || request.getEmail().trim().isEmpty() ? env.getProperty("visanet.default.email") : request.getEmail());
        authRequest.setInstallments("0");
        authRequest.setExpirationMonth(expireMonth);
        authRequest.setExpirationYear(expireYear);
        authRequest.setExternalTransactionId(nf.format(tx.getId()));
        authRequest.setFirstName(request.getCardHolderFirstName().trim());
        authRequest.setLastName(request.getCardHolderLastName().trim());
        authRequest.setMerchantId(merchantId);
        authRequest.setQremv(request.getQrImageBase64());

        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - QR Content= " + stringDecoder.hexNibblesToSString(request.getQrImageBase64()));

        NumberFormat nfPurchase = new DecimalFormat(env.getProperty("visanet.purchasenumber.format"));
        String purchaseNumber = env.getProperty("visanet.purchasenumber.prefix") + nfPurchase.format(tx.getId());
        authRequest.setPurchaseNumber(purchaseNumber);
        tx.setVisanetRequest(Crypto.aesEncrypt(objectMapper.writeValueAsString(authRequest)));

        CardAutorizationResponse authResponse;

        try {
            authResponse = apiLogin.authorizePlainCard(token, authRequest);
        } catch (Exception e) {
            LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - Exception consuming auth plain card service " + e);
            bitTicket += ",resultado= TRANSACCIÓN FALLIDA";
            bitacora.setTicket(bitTicket);
            bitacora.setStatus(-1);
            tx.setStatus(8L);
            tx.setAuthCardMessage(e.getMessage());
            tx.setServiceResponse(Crypto.aesEncrypt(objectMapper.writeValueAsString(new BaseApiResponse(-1L, "VisaQrWalletService - callback - handleException - Exception in service: Error in consuming authorization plain card service in visanet gateway. Server error: " + e.getMessage()))));
            visaNetRepository.save(tx);
            tBitacoraRepository.save(bitacora);
            throw new Exception("Error in consuming authorization plain card service in visanet gateway. Server error: " + e.getMessage());
        }

        tx.setVisanetResponse(Crypto.aesEncrypt(objectMapper.writeValueAsString(authResponse)));

        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - Visanet authorize plain card  service returned code="
                + (authResponse == null ? "null" : authResponse.getCodeResponse()));

        if (authResponse == null || Long.valueOf(authResponse.getCodeResponse()) != 0L) {
            if (authResponse == null) {
                bitTicket += ",resultado=Error en gateway visanet respuesta = null";
            } else {
                bitTicket += ",resultado=Error en gateway visanet code=" + authResponse.getCodeResponse() + ", message=" + authResponse.getMessage();
            }
            bitacora.setTicket(bitTicket);
            bitacora.setStatus(-1);
            tx.setStatus(8L);
            tx.setAuthCardCode(authResponse.getCodeResponse() == null ? null : Long.valueOf(authResponse.getCodeResponse()));
            tx.setAuthCardMessage(authResponse.getMessage());
            tx.setServiceResponse(Crypto.aesEncrypt(objectMapper.writeValueAsString(new BaseApiResponse(-1L, "VisaQrWalletService - callback - handleException - Exception in service: " + "Error consuming Visa Integration API Generate QR code=" + authResponse.getCodeResponse() + " message=" + authResponse.getMessage()))));
            visaNetRepository.save(tx);
            tBitacoraRepository.save(bitacora);
            throw new Exception(
                    "Error consuming Visa Integration API Generate QR code=" + authResponse.getCodeResponse() + " message=" + authResponse.getMessage());
        }

        // actualizar transaccion
        tx.setTransactionType(tx.getTransactionType() + 100L);
        tx.setStatus(2L);
        tx.setCardHolderFirstName(request.getCardHolderFirstName());
        tx.setCardHolderLastName(request.getCardHolderLastName());
        tx.setAuthCardDate(Calendar.getInstance().getTime());
        tx.setAuthCardCode(Long.valueOf(authResponse.getCodeResponse()));
        tx.setAuthCardMessage(authResponse.getMessage());
        tx.setIdBitacora(bitacora.getId());
        tx.setCardId(request.getCardId());
        tx.setIdUser(tdc == null ? null : tdc.getIdUsuario());
        CardAutorizationResult cardAuthResult = authResponse.getResult();
        if (cardAuthResult != null) {
            CardAuthorizationHeader header = cardAuthResult.getHeader();
            if (header != null) {
                tx.setEcoreTransactionUuid(cardAuthResult.getHeader().getEcoreTransactionUuid());
                tx.setEcoreTransactionDate(header.getEcoreTransactionDate());
                tx.setEcoreMillis(Long.valueOf(header.getMillis()));
            }

            CardAuthorizationOrder order = cardAuthResult.getOrder();
            if (order != null) {
                tx.setOrderPurchaseNumber(order.getPurchaseNumber());
                tx.setOrderAmount(order.getAmount());
                tx.setOrderInstallment(order.getInstallment());
                tx.setOrderCurrency(order.getCurrency());
                tx.setOrderExternalTransactionId(order.getExternalTransactionId());
                tx.setOrderAuthorizedAmount(order.getAuthorizedAmount());
                tx.setOrderActionCode(order.getActionCode());
                tx.setOrderTraceNumber(order.getTraceNumber());
                tx.setOrderTransactionDate(order.getTransactionDate());
                tx.setOrderTransactionId(order.getTransactionId());
                tx.setOrderAuthorizationCode(order.getAuthorizationCode());
                bitacora.setNumeroAutorizacion(order.getAuthorizationCode());
            }

            CardAuthorizationDataMap map = cardAuthResult.getDataMap();
            if (map != null) {
                tx.setMapIdUnico(map.getIdUnico());
                tx.setMapMerchant(map.getMerchant());
                tx.setMapAuthorizationCode(map.getAuthCode());
                tx.setMapCard(map.getCard());
                tx.setMapCurrency(map.getCurrency());
                tx.setMapTerminal(map.getTerminal());
                tx.setMapTransactionDate(map.getTransactionDate());
                tx.setMapActionCode(map.getActionCode());
                tx.setMapStatus(map.getStatus());
                tx.setMapActionDescription(map.getActionDescription());
                tx.setMapAdquiriente(map.getAdquiriente());
                tx.setMapAmount(map.getAmount());
                tx.setMapProcessCode(map.getProcessCode());
                tx.setMapTransactionId(map.getTransactionId());
                tx.setMapTraceNumber(map.getTraceNumber());
            }
        }


        // guardar tBitacora
        bitTicket += ",resultado= TRANSACCIÓN EXITOSA";
        bitacora.setStatus(1);
        bitacora.setTicket(bitTicket);
        tBitacoraRepository.save(bitacora);

        CardAuthFromQrResponse cardResponse = new CardAuthFromQrResponse(0L, "successful", bitacora.getId(), tx.getId(), tx.getOrderPurchaseNumber(),
                tx.getOrderAuthorizationCode(), tx.getMapTransactionDate());
        tx.setServiceResponse(Crypto.aesEncrypt(objectMapper.writeValueAsString(cardResponse)));
        visaNetRepository.save(tx);

        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - service return OK");
        return new ResponseEntity<>(cardResponse, HttpStatus.OK);
    }

    @RequestMapping(value = "{idApp}/{idCountry}/{language}/{businessId}/card/authorization/manual", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ManualCardAuthFromQrResponse> authorizePaymentQrCardManual(@PathVariable("idApp") Long applicationId,
                                                                                     @PathVariable("idCountry") Long countryId, @PathVariable("language") String language, @PathVariable("businessId") Long businessId,
                                                                                     @RequestBody ManualCardAuthFromQrRequest request) throws Exception {

        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardManual init - applicationId=" + applicationId + ", countryId=" + countryId
                + ", language=" + language + ", businessId" + businessId);

        Boolean businessIdExistsInBd = businessId != null && lcpfEstablecimientoReposiotry.existsById(businessId);


        if (request.getQrImageBase64() == null || request.getQrImageBase64().trim().isEmpty()) {
            throw new Exception("A base64 encoded QR image is required");
        }

        if (request.getCardHolderFirstName() == null || request.getCardHolderFirstName().trim().isEmpty()) {
            throw new Exception("A Card holder First Name is required");
        }

        if (request.getCardHolderLastName() == null || request.getCardHolderLastName().trim().isEmpty()) {
            throw new Exception("A Card holder Last Name is required");
        }

        if (request.getCardNumber() == null || request.getCardNumber().trim().isEmpty()) {
            throw new Exception("A Card Number is required");
        }

        if (request.getCardCvv() == null || request.getCardCvv().trim().isEmpty()) {
            throw new Exception("A Card Cvv is required");
        }

        if (request.getExpirationMonth() == null) {
            throw new Exception("A Card Expiration month is required.");
        }

        if (request.getExpirationYear() == null) {
            throw new Exception("A Card Expiration year is required.");
        }

        DecodedQrContentDTO decodedQr = qrEmvDecoder.decodeQrCodeHex(request.getQrImageBase64());

        //si el tipo de qr es estático error pues debe especificarse monto en el qr
        if (decodedQr.getType() == null || (decodedQr.getType().equals(env.getProperty("visanet.qr.type.static")))) {
            throw new Exception("Invalid QR Type =" + decodedQr.getType() + ", QR Must be dynamic and specify an amount");
        }


        // url base para guardar en BD
        String baseUrl = String.format("%d/%d/%s/%d/card/authorization/manual", applicationId, countryId, language, businessId);
        String paymentType = "cardManual";

        final ObjectMapper objectMapper = new ObjectMapper();

        // login en API
        String token;
        try {
            token = apiLogin.generateLoginToken();
            if (token == null) {
                throw new Exception("Unable to generate API Security Token.");
            }
        } catch (Exception e) {
            LOGGER.info("VisaQrWalletService - authorizePaymentQrCardManual - Exception generating security token " + e);
            throw new Exception("Error in consuming auth service in visanet gateway. Server error: " + e.getMessage());
        }
        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardManual - Visanet auth token created");

        String businessName = decodedQr.getBusinessName();
        BigDecimal amount = decodedQr.getAmount().setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal comision = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal totalAmount = amount.add(comision);
        String description = request.getDescription() == null || request.getDescription().trim().isEmpty() ? "Transacción TDC Manual" : request.getDescription().trim();


        // buscar la transaccion por md5
        VisanetTransaction tx = new VisanetTransaction();
        tx.setIdApplication(applicationId);
        tx.setIdCountry(countryId);
        tx.setLanguage(language);
        tx.setAmount(totalAmount);
        tx.setComision(comision);
        tx.setDescription(description);
        tx.setQrExpirationDate(null);
        tx.setIdUser(null);
        tx.setTransactionType(21L);
        tx.setIdLcpfEstablecimiento(businessId);
        tx.setPaymentType(paymentType);
        tx.setAuthUrl(baseUrl);
        tx.setServiceRequest(Crypto.aesEncrypt(objectMapper.writeValueAsString(request)));
        tx.setEmail(request.getEmail() == null || request.getEmail().trim().isEmpty() ? env.getProperty("visanet.default.email") : request.getEmail());
        tx.setCardType(request.getCardType());
        visaNetRepository.save(tx);


        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardManual - QR transaction has a valid status=1");

        String tdcNumber = request.getCardNumber();

        NumberFormat nf = new DecimalFormat("00");
        String expireMonth = request.getExpirationMonth().toString(); //nf.format(request.getExpirationMonth());
        String expireYear = nf.format(request.getExpirationYear());

        if (expireYear.length() > 2) {
            expireYear = expireYear.substring(expireYear.length() - 2);
        }


        // crear bitacora
        String maskedCard = creditCardNumberFormatter.maskedCardNumber(tdcNumber);

        String bitTicket = "PAGO VISANET CON TARJETA DE CRÉDITO, A NOMBRE DEL COMERCIO: " + businessName.toUpperCase() + ", TARJETA #" + maskedCard + ", MONTO S. " + decodedQr.getAmount().toPlainString();

        TBitacora bitacora = new TBitacora();
        bitacora.setIdUsuario(null);
        bitacora.setIdioma(language);
        bitacora.setIdAplicacion(applicationId);
        bitacora.setIdProveedor(700L);
        bitacora.setIdProducto(7100L);
        Date bitfecha = Calendar.getInstance().getTime();
        bitacora.setFecha(bitfecha);
        bitacora.setHora(bitfecha);
        bitacora.setConcepto(tx.getDescription());
        bitacora.setCargo(decodedQr.getAmount());
        bitacora.setComision(BigDecimal.ZERO);
        bitacora.setStatus(1);
        bitacora.setTicket(bitTicket);
        bitacora.setTarjetaCompra(Crypto.aesEncrypt(request.getCardNumber()));
        bitacora.setIdEstablecimiento(businessId);
        tBitacoraRepository.save(bitacora);


        nf = new DecimalFormat("000000000000000000000000000000000000");
        CardAutorizationRequest authRequest = new CardAutorizationRequest();
        authRequest.setCardNumber(tdcNumber);
        authRequest.setCvv2Code("0");
        authRequest.setCreateAlias(Boolean.FALSE.toString());
        authRequest.setAmount(decodedQr.getAmount().toPlainString());
        authRequest.setCurrencyId(env.getProperty("visanet.api.currency.pen.code").trim());
        authRequest.setEmail(request.getEmail() == null || request.getEmail().trim().isEmpty() ? env.getProperty("visanet.default.email") : request.getEmail());
        authRequest.setInstallments("0");
        authRequest.setExpirationMonth(expireMonth);
        authRequest.setExpirationYear(expireYear);
        authRequest.setExternalTransactionId(nf.format(tx.getId()));
        authRequest.setFirstName(request.getCardHolderFirstName().trim());
        authRequest.setLastName(request.getCardHolderLastName().trim());
        authRequest.setMerchantId(env.getProperty("visanet.api.merchantid").trim());
        authRequest.setQremv(request.getQrImageBase64());

        NumberFormat nfPurchase = new DecimalFormat(env.getProperty("visanet.purchasenumber.format"));
        String purchaseNumber = env.getProperty("visanet.purchasenumber.prefix") + nfPurchase.format(tx.getId());
        authRequest.setPurchaseNumber(purchaseNumber);
        tx.setVisanetRequest(Crypto.aesEncrypt(objectMapper.writeValueAsString(authRequest)));

        CardAutorizationResponse authResponse;

        try {
            authResponse = apiLogin.authorizePlainCard(token, authRequest);
        } catch (Exception e) {
            LOGGER.info("VisaQrWalletService - authorizePaymentQrCardManual - Exception consuming auth plain card service " + e);
            bitTicket += " -- TRANSACCIÓN RECHAZADA --";
            bitacora.setTicket(bitTicket);
            bitacora.setStatus(-1);
            tx.setStatus(8L);
            tx.setAuthCardMessage(e.getMessage());
            tx.setServiceResponse(Crypto.aesEncrypt(objectMapper.writeValueAsString(new BaseApiResponse(-1L, "VisaQrWalletService - handleException - Exception in service: " + " Error in consuming authorization plain card service in visanet gateway. Server error: " + e.getMessage()))));
            visaNetRepository.save(tx);
            tBitacoraRepository.save(bitacora);
            throw new Exception("Error in consuming authorization plain card service in visanet gateway. Server error: " + e.getMessage());
        }
        tx.setServiceResponse(Crypto.aesEncrypt(objectMapper.writeValueAsString(authResponse)));
        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardFromDB - Visanet authorize plain card  service returned code="
                + (authResponse == null ? "null" : authResponse.getCodeResponse()));

        if (authResponse == null || Long.valueOf(authResponse.getCodeResponse()) != 0L) {
            bitTicket += " -- TRANSACCIÓN RECHAZADA --";

            bitacora.setTicket(bitTicket);
            bitacora.setStatus(-1);
            tx.setStatus(8L);
            tx.setAuthCardCode(authResponse.getCodeResponse() == null ? null : Long.valueOf(authResponse.getCodeResponse()));
            tx.setAuthCardMessage(authResponse.getMessage());
            tx.setServiceResponse(Crypto.aesEncrypt(objectMapper.writeValueAsString(new BaseApiResponse(-1L, "Error consuming Visa Integration API Generate QR code=" + authResponse.getCodeResponse() + " message=" + authResponse.getMessage()))));
            visaNetRepository.save(tx);
            tBitacoraRepository.save(bitacora);
            throw new Exception(
                    "Error consuming Visa Integration API Generate QR code=" + authResponse.getCodeResponse() + " message=" + authResponse.getMessage());
        }
        tx.setVisanetResponse(Crypto.aesEncrypt(objectMapper.writeValueAsString(authResponse)));


        // actualizar transaccion
        tx.setTransactionType(tx.getTransactionType() + 200L);
        tx.setStatus(2L);
        tx.setCardHolderFirstName(request.getCardHolderFirstName());
        tx.setCardHolderLastName(request.getCardHolderLastName());
        tx.setAuthCardDate(Calendar.getInstance().getTime());
        tx.setAuthCardCode(Long.valueOf(authResponse.getCodeResponse()));
        tx.setAuthCardMessage(authResponse.getMessage());
        tx.setIdBitacora(bitacora.getId());
        tx.setCardNumber(Crypto.aesEncrypt(request.getCardNumber()));
        tx.setCardCvv(Crypto.aesEncrypt(request.getCardCvv()));
        tx.setIdUser(null);
        tx.setCardExpirationDate(Crypto.aesEncrypt(expireMonth + "/" + expireYear));
        tx.setBusinessName(businessName);

        CardAutorizationResult cardAuthResult = authResponse.getResult();
        if (cardAuthResult != null) {
            CardAuthorizationHeader header = cardAuthResult.getHeader();
            if (header != null) {
                tx.setEcoreTransactionUuid(cardAuthResult.getHeader().getEcoreTransactionUuid());
                tx.setEcoreTransactionDate(header.getEcoreTransactionDate());
                tx.setEcoreMillis(Long.valueOf(header.getMillis()));
            }

            CardAuthorizationOrder order = cardAuthResult.getOrder();
            if (order != null) {
                tx.setOrderPurchaseNumber(order.getPurchaseNumber());
                tx.setOrderAmount(order.getAmount());
                tx.setOrderInstallment(order.getInstallment());
                tx.setOrderCurrency(order.getCurrency());
                tx.setOrderExternalTransactionId(order.getExternalTransactionId());
                tx.setOrderAuthorizedAmount(order.getAuthorizedAmount());
                tx.setOrderActionCode(order.getActionCode());
                tx.setOrderTraceNumber(order.getTraceNumber());
                tx.setOrderTransactionDate(order.getTransactionDate());
                tx.setOrderTransactionId(order.getTransactionId());
                tx.setOrderAuthorizationCode(order.getAuthorizationCode());
                bitacora.setNumeroAutorizacion(order.getAuthorizationCode());
            }

            CardAuthorizationDataMap map = cardAuthResult.getDataMap();
            if (map != null) {
                tx.setMapIdUnico(map.getIdUnico());
                tx.setMapMerchant(map.getMerchant());
                tx.setMapAuthorizationCode(map.getAuthCode());
                tx.setMapCard(map.getCard());
                tx.setMapCurrency(map.getCurrency());
                tx.setMapTerminal(map.getTerminal());
                tx.setMapTransactionDate(map.getTransactionDate());
                tx.setMapActionCode(map.getActionCode());
                tx.setMapStatus(map.getStatus());
                tx.setMapActionDescription(map.getActionDescription());
                tx.setMapAdquiriente(map.getAdquiriente());
                tx.setMapAmount(map.getAmount());
                tx.setMapProcessCode(map.getProcessCode());
                tx.setMapTransactionId(map.getTransactionId());
                tx.setMapTraceNumber(map.getTraceNumber());
            }
        }


        // guardar tBitacora
        bitTicket += " -- TRANSACCION APROBADA -- ";
        bitacora.setStatus(1);
        bitacora.setTicket(bitTicket);
        tBitacoraRepository.save(bitacora);

        ManualCardAuthFromQrResponse cardResponse = new ManualCardAuthFromQrResponse(0L, "successful", bitacora.getId(), tx.getId(), tx.getOrderPurchaseNumber(),
                tx.getOrderAuthorizationCode(), tx.getMapTransactionDate());
        tx.setServiceResponse(Crypto.aesEncrypt(objectMapper.writeValueAsString(cardResponse)));
        visaNetRepository.save(tx);

        LOGGER.info("VisaQrWalletService - authorizePaymentQrCardManual - service return OK");
        return new ResponseEntity<>(cardResponse, HttpStatus.OK);
    }


}
