package com.addcel.visanet.qrwallet.dto;

import java.io.Serializable;

public class CreatePaymentQrResponse extends BaseApiResponse implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String qrTag;
    
    private String qrBase64;
    
    public  CreatePaymentQrResponse () {
        super();
    }


    public CreatePaymentQrResponse(Long code, String message, String qrTag, String qrBase64) {
        super();
        this.code = code;
        this.message = message;
        this.qrBase64 = qrBase64;
        this.qrTag = qrTag;
    }

    public String getQrBase64() {
        return qrBase64;
    }

    public void setQrBase64(String qrBase64) {
        this.qrBase64 = qrBase64;
    }

    public String getQrTag() {
        return qrTag;
    }

    public void setQrTag(String qrTag) {
        this.qrTag = qrTag;
    }

}
