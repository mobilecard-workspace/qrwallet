package com.addcel.visanet.qrwallet.dto;

import java.io.Serializable;

public class DecodeQrHexaRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    private String qrHexa;

    public DecodeQrHexaRequest() {
        super();
    }

    public DecodeQrHexaRequest(String qrHexa) {
        super();
        this.qrHexa = qrHexa;
    }

    public String getQrHexa() {
        return qrHexa;
    }

    public void setQrHexa(String qrHexa) {
        this.qrHexa = qrHexa;
    }
}
