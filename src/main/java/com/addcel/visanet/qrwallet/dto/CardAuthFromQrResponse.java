package com.addcel.visanet.qrwallet.dto;

import com.fasterxml.jackson.databind.ser.Serializers;

import java.io.Serializable;
import java.util.Date;

public class CardAuthFromQrResponse  extends BaseApiResponse implements Serializable {
    
    private static final long serialVersionUID = 1L;

    private Long bitacoraId;

    private Long transactionId;

    private String purchaseNumber;

    private String authorizationCode;

    private String transactionDate;

    public CardAuthFromQrResponse () {
        super ();
    }

    public CardAuthFromQrResponse(Long code, String message, Long bitacoraId, Long transactionId, String purchaseNumber, String authorizationCode, String transactionDate) {
        this.code = code;
        this.message = message;
        this.bitacoraId = bitacoraId;
        this.transactionId = transactionId;
        this.purchaseNumber = purchaseNumber;
        this.authorizationCode = authorizationCode;
        this.transactionDate = transactionDate;
    }

    public Long getBitacoraId() {
        return bitacoraId;
    }

    public void setBitacoraId(Long bitacoraId) {
        this.bitacoraId = bitacoraId;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getPurchaseNumber() {
        return purchaseNumber;
    }

    public void setPurchaseNumber(String purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }
}
