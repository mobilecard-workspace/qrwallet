package com.addcel.visanet.qrwallet.dto;

import java.io.Serializable;

public class VisaNetCallbackRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private Boolean successful;

    private String merchantId;

    private String merchantName;

    private String transactionCurrency;

    private String transactionCountry;

    private String mapIdUnico;

    private String mapAuthorizationCode;

    private String mapCard;

    private String mapTerminal;

    private String mapTransactionDate;

    private String mapActionCode;

    private String mapStatus;

    private String mapActionDescription;

    private String mapAdquiriente;

    private String mapAmount;

    private String mapTransactionId;

    private String aditionalInfo;

    private String mapProcessCode;

    private String qrTagId;

    public VisaNetCallbackRequest () {
        super ();
    }


    public Boolean getSuccessful() {
        return successful;
    }

    public void setSuccessful(Boolean successful) {
        this.successful = successful;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    public void setTransactionCurrency(String transactionCurrency) {
        this.transactionCurrency = transactionCurrency;
    }

    public String getMapIdUnico() {
        return mapIdUnico;
    }

    public void setMapIdUnico(String mapIdUnico) {
        this.mapIdUnico = mapIdUnico;
    }

    public String getMapAuthorizationCode() {
        return mapAuthorizationCode;
    }

    public void setMapAuthorizationCode(String mapAuthorizationCode) {
        this.mapAuthorizationCode = mapAuthorizationCode;
    }

    public String getMapCard() {
        return mapCard;
    }

    public void setMapCard(String mapCard) {
        this.mapCard = mapCard;
    }

    public String getMapTerminal() {
        return mapTerminal;
    }

    public void setMapTerminal(String mapTerminal) {
        this.mapTerminal = mapTerminal;
    }

    public String getMapTransactionDate() {
        return mapTransactionDate;
    }

    public void setMapTransactionDate(String mapTransactionDate) {
        this.mapTransactionDate = mapTransactionDate;
    }

    public String getMapActionCode() {
        return mapActionCode;
    }

    public void setMapActionCode(String mapActionCode) {
        this.mapActionCode = mapActionCode;
    }

    public String getMapStatus() {
        return mapStatus;
    }

    public void setMapStatus(String mapStatus) {
        this.mapStatus = mapStatus;
    }

    public String getMapActionDescription() {
        return mapActionDescription;
    }

    public void setMapActionDescription(String mapActionDescription) {
        this.mapActionDescription = mapActionDescription;
    }

    public String getMapAdquiriente() {
        return mapAdquiriente;
    }

    public void setMapAdquiriente(String mapAdquiriente) {
        this.mapAdquiriente = mapAdquiriente;
    }

    public String getMapAmount() {
        return mapAmount;
    }

    public void setMapAmount(String mapAmount) {
        this.mapAmount = mapAmount;
    }

    public String getMapTransactionId() {
        return mapTransactionId;
    }

    public void setMapTransactionId(String mapTransactionId) {
        this.mapTransactionId = mapTransactionId;
    }

    public String getAditionalInfo() {
        return aditionalInfo;
    }

    public void setAditionalInfo(String aditionalInfo) {
        this.aditionalInfo = aditionalInfo;
    }

    public String getTransactionCountry() {
        return transactionCountry;
    }

    public void setTransactionCountry(String transactionCountry) {
        this.transactionCountry = transactionCountry;
    }

    public String getMapProcessCode() {
        return mapProcessCode;
    }

    public void setMapProcessCode(String mapProcessCode) {
        this.mapProcessCode = mapProcessCode;
    }

    public String getQrTagId() {
        return qrTagId;
    }

    public void setQrTagId(String qrTagId) {
        this.qrTagId = qrTagId;
    }
}
