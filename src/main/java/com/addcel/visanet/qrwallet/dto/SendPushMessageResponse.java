package com.addcel.visanet.qrwallet.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class SendPushMessageResponse extends BaseApiResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private String idPush;

    private String successDate;

    public SendPushMessageResponse () {
        super();
    }

    public SendPushMessageResponse(Long code, String message) {
        super(code, message);
    }

    public SendPushMessageResponse(Long code, String message, String idPush, String successDate) {
        super(code, message);
        this.idPush = idPush;
        this.successDate = successDate;
    }

    public String getSuccessDate() {
        return successDate;
    }

    public void setSuccessDate(String successDate) {
        this.successDate = successDate;
    }
}
