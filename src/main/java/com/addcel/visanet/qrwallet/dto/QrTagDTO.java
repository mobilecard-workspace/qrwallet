package com.addcel.visanet.qrwallet.dto;

public class QrTagDTO {
    private int initPos;

    private int endPos;

    private String code;

    private String content;

    public QrTagDTO() {
        super();
    }

    public QrTagDTO(int initPos, int endPos, String code, String content) {
        this.initPos = initPos;
        this.endPos = endPos;
        this.code = code;
        this.content = content;
    }

    public int getInitPos() {
        return initPos;
    }

    public void setInitPos(int initPos) {
        this.initPos = initPos;
    }

    public int getEndPos() {
        return endPos;
    }

    public void setEndPos(int endPos) {
        this.endPos = endPos;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("QrTag{");
        sb.append("initPos=").append(initPos);
        sb.append(", endPos=").append(endPos);
        sb.append(", code='").append(code).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
