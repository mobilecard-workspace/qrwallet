package com.addcel.visanet.qrwallet.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ManualCardAuthFromQrRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private String qrImageBase64;

    private String cardHolderFirstName;

    private String cardHolderLastName;

    private String cardNumber;
    
    private String cardCvv;
    
    private Integer expirationMonth;
    
    private Integer expirationYear;
    
    private String businessName;

    private String description;

    private String email;

    private String cardType;
    
    
    public ManualCardAuthFromQrRequest () {
        super ();
    }



    public ManualCardAuthFromQrRequest(String qrImageBase64, String cardHolderFirstName, String cardHolderLastName, String cardNumber, String cardCvv,
            Integer expirationMonth, Integer expirationYear) {
        super();
        this.qrImageBase64 = qrImageBase64;
        this.cardHolderFirstName = cardHolderFirstName;
        this.cardHolderLastName = cardHolderLastName;
        this.cardNumber = cardNumber;
        this.cardCvv = cardCvv;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
    }



    public String getQrImageBase64() {
        return qrImageBase64;
    }

    public void setQrImageBase64(String qrImageBase64) {
        this.qrImageBase64 = qrImageBase64;
    }

    public String getCardHolderFirstName() {
        return cardHolderFirstName;
    }

    public void setCardHolderFirstName(String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }

    public String getCardHolderLastName() {
        return cardHolderLastName;
    }

    public void setCardHolderLastName(String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(String cardCvv) {
        this.cardCvv = cardCvv;
    }

    public Integer getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(Integer expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public Integer getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(Integer expirationYear) {
        this.expirationYear = expirationYear;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
}