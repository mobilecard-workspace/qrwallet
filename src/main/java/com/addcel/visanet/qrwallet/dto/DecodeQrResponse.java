package com.addcel.visanet.qrwallet.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.math.BigDecimal;

public class DecodeQrResponse extends BaseApiResponse implements Serializable {


    private static final long serialVersionUID = 1L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int version;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String type;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String countryCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String city;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String businessName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String currencyCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String merchantId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal amount;

    public DecodeQrResponse() {
        super ();
    }

    public DecodeQrResponse(Long code, String message) {
        super(code, message);
    }

    public DecodeQrResponse(Long code, String message, DecodedQrContentDTO qrContent) {
        super(code, message);
        this.id = qrContent.getId();
        this.version = qrContent.getVersion();
        this.type = qrContent.getType();
        this.countryCode = qrContent.getCountryCode();
        this.city = qrContent.getCity();
        this.businessName = qrContent.getBusinessName();
        this.currencyCode = qrContent.getCurrencyCode();
        this.merchantId = qrContent.getMerchantId();
        this.amount = qrContent.getAmount();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
