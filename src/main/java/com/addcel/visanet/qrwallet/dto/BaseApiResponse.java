package com.addcel.visanet.qrwallet.dto;

import java.io.Serializable;

public class BaseApiResponse implements Serializable  {
    
    private static final long serialVersionUID = 1L;
    
    protected Long code;
    
    protected String message;

    public BaseApiResponse () {
        super();
    }

    public BaseApiResponse(Long code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    
}
