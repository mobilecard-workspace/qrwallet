package com.addcel.visanet.qrwallet.dto;

import java.io.Serializable;

public class DecodeQrAsciiRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    private String qrAscii;

    public DecodeQrAsciiRequest() {
        super();
    }

    public DecodeQrAsciiRequest(String qrAscii) {
        this.qrAscii = qrAscii;
    }

    public String getQrAscii() {
        return qrAscii;
    }

    public void setQrAscii(String qrAscii) {
        this.qrAscii = qrAscii;
    }
}
