package com.addcel.visanet.qrwallet.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.math.BigDecimal;

public class DecodedQrContentDTO implements Serializable {


    private static final long serialVersionUID = 1L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int version;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String type;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String countryCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String city;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String businessName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String currencyCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String merchantId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal amount;


    public DecodedQrContentDTO() {
        super ();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DecodedQrContentDTO{");
        sb.append("id='").append(id).append('\'');
        sb.append(", version=").append(version);
        sb.append(", type='").append(type).append('\'');
        sb.append(", countryCode='").append(countryCode).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", businessName='").append(businessName).append('\'');
        sb.append(", currencyCode='").append(currencyCode).append('\'');
        sb.append(", merchantId='").append(merchantId).append('\'');
        sb.append(", amount=").append(amount);
        sb.append('}');
        return sb.toString();
    }
}
