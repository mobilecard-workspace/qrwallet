package com.addcel.visanet.qrwallet.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SendPushMessageRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long modulo;

    @JsonProperty("id_usuario")
    private Long idUsuario;

    private String tipoUsuario;

    private String idioma;

    private Long idPais;

    private Long idApp;

    private List<SendPushMessageParam> params;

    public SendPushMessageRequest() {
        super();
        this.params = new ArrayList<>();
    }

    public Long getModulo() {
        return modulo;
    }

    public void setModulo(Long modulo) {
        this.modulo = modulo;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public Long getIdPais() {
        return idPais;
    }

    public void setIdPais(Long idPais) {
        this.idPais = idPais;
    }

    public Long getIdApp() {
        return idApp;
    }

    public void setIdApp(Long idApp) {
        this.idApp = idApp;
    }

    public List<SendPushMessageParam> getParams() {
        return params;
    }

    public void setParams(List<SendPushMessageParam> params) {
        this.params = params;
    }
}
