package com.addcel.visanet.qrwallet.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CardAuthFromQrRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private String qrImageBase64;

    private Long cardId;

    private String cardHolderFirstName;

    private String cardHolderLastName;

    private BigDecimal baseAmount;

    private BigDecimal comision;

    private String description;

    private String email;

    private String cardType ;


    public CardAuthFromQrRequest () {
        super ();
    }

    public CardAuthFromQrRequest(String qrImageBase64, Long cardId, String cardHolderFirstName, String cardHolderLastName) {
        this.qrImageBase64 = qrImageBase64;
        this.cardId = cardId;
        this.cardHolderFirstName = cardHolderFirstName;
        this.cardHolderLastName = cardHolderLastName;
    }

    public String getQrImageBase64() {
        return qrImageBase64;
    }

    public void setQrImageBase64(String qrImageBase64) {
        this.qrImageBase64 = qrImageBase64;
    }

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public String getCardHolderFirstName() {
        return cardHolderFirstName;
    }

    public void setCardHolderFirstName(String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }

    public String getCardHolderLastName() {
        return cardHolderLastName;
    }

    public void setCardHolderLastName(String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }

    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    public BigDecimal getComision() {
        return comision;
    }

    public void setComision(BigDecimal comision) {
        this.comision = comision;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
}
