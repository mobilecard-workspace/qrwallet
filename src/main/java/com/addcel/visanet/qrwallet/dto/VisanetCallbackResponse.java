package com.addcel.visanet.qrwallet.dto;

import java.io.Serializable;

public class VisanetCallbackResponse extends BaseApiResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public VisanetCallbackResponse () {
        super();
    }
    
    public VisanetCallbackResponse(Long code, String message) {
        super(code, message);
    }

    

}
