package com.addcel.visanet.qrwallet.components;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

@Component
public class QRReader {

    private final Logger LOGGER = LogManager.getLogger(QRReader.class);

    public QRReader () {
        super();
    }
    
    public String readQR(final BufferedImage bufferedImage) throws Exception {
        LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        try {
            Result result = new MultiFormatReader().decode(bitmap);
            return result.getText();
        } catch (NotFoundException e) {
            LOGGER.info("Excepción cargando qr");
            e.printStackTrace();
            return null;
        }
    }

    public String readQRFromPNG(final File qrPath) throws Exception  {
        LOGGER.info("Leyendo qr del archivo " + qrPath.getAbsolutePath());
        return readQR(ImageIO.read(qrPath));
        
    }
    
    public String readQRFromPNG(final byte[] pngFile) throws Exception  {
        try (InputStream in = new ByteArrayInputStream(pngFile)) {
            BufferedImage img = ImageIO.read(in);
            LOGGER.info("cargado BufferedImage. Ancho=" + img.getWidth() + " Alto=" + img.getHeight());
            return readQR(img);
        }
    }

    public String calculateQrContentMd5FromBase64Img(String imageBase64) throws Exception {
        String qrContent = readQRFromPNG(Base64.getDecoder().decode(imageBase64));
        LOGGER.info("qrcontnet=" + qrContent);
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(qrContent.getBytes());
        return DatatypeConverter.printHexBinary(messageDigest.digest()).toLowerCase();
    }

}
