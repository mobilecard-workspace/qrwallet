package com.addcel.visanet.qrwallet.components;

import com.addcel.visanet.qrwallet.dto.SendPushMessageParam;
import com.addcel.visanet.qrwallet.dto.SendPushMessageRequest;
import com.addcel.visanet.qrwallet.dto.SendPushMessageResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class PushServiceConsumer {

    private static final Logger LOGGER = LogManager.getLogger(PushServiceConsumer.class);

    @Autowired
    Environment env;


    public PushServiceConsumer() {
        super();
    }

    public SendPushMessageRequest createSendPushMessageRequest(Long businessId, Long countryId, String languageId, Long appId, String amount) {
        // objeto request
        SendPushMessageRequest request = new SendPushMessageRequest();
        request.setModulo(Long.valueOf(env.getProperty("mobilecard.push.module").trim()));
        request.setIdUsuario(businessId);
        request.setTipoUsuario(env.getProperty("mobilecard.push.user.type").trim());
        request.setIdioma(languageId);
        request.setIdPais(countryId);
        request.setIdApp(appId);
        // array parámetros
        // parametro <tag>
        request.getParams().add(new SendPushMessageParam(env.getProperty("mobilecard.push.tag.name").trim(), env.getProperty("mobilecard.push.tag.value").trim()));
        // parametro <monto>
        request.getParams().add(new SendPushMessageParam(env.getProperty("mobilecard.push.tag.amount").trim(), amount));
        return request;
    }

    public void pushNotification(Long businessId, Long countryId, String languageId, Long appId, String amount) {
        LOGGER.info("PushServiceConsumer.pushNotification - inicio método businessId=" + businessId + ", countryId=" + countryId + ",languageId=" + languageId + ", appId=" + appId + ",  amount=" + amount);
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse apiResponse = null;
        try {
            String url = env.getProperty("mobilecard.push.url").trim();
            LOGGER.info("PushServiceConsumer.pushNotification - URL a consumir  = " + url);
            // armar el request
            SendPushMessageRequest request = createSendPushMessageRequest(businessId, countryId, languageId, appId, amount);
            ObjectMapper mapper = new ObjectMapper();
            LOGGER.info("VisaNetApi.authorizePlainCard - RequestBody=" + mapper.writeValueAsString(request));

            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new StringEntity(mapper.writeValueAsString(request)));
            httpPost.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION, env.getProperty("mobilecard.push.auth.token").trim()));
            httpPost.addHeader(new BasicHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE));
            httpPost.addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE));

            httpClient = HttpClients.createDefault();
            apiResponse = httpClient.execute(httpPost);
            LOGGER.info("PushServiceConsumer.pushNotification - Codigo de response " + apiResponse.getStatusLine().getStatusCode() + ": " + apiResponse.getStatusLine().getReasonPhrase());

            if (apiResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                SendPushMessageResponse response = mapper.readValue(apiResponse.getEntity().getContent(), SendPushMessageResponse.class);
                Long code = response == null || response.getCode() == null ? -1L : response.getCode();
                LOGGER.info("VisaNetApi.authorizePlainCard - ResponseBody=" + mapper.writeValueAsString(response));
                if (code == 0) {
                    LOGGER.info("VisaNetApi.authorizePlainCard - El mensaje push fue enviado al comercio");
                } else {
                    LOGGER.info("VisaNetApi.authorizePlainCard - El mensaje push no pudo ser enviado al comercio");
                }
            } else {
                LOGGER.info("PushServiceConsumer.pushNotification - el servicio devolvio mensaje error=" + apiResponse.getStatusLine().getStatusCode() + ", descripcion=" + apiResponse.getStatusLine().getReasonPhrase() + ", no se pudo hacer el psuh");
            }
            httpClient.close();
            apiResponse.close();
            LOGGER.info("PushServiceConsumer.pushNotification - fin metodo");
        } catch (Exception e) {
            LOGGER.info("PushServiceConsumer.pushNotification - excepcion al enviar notificacion push " + e);
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();
            } catch (Exception e) {

            }

            try {
                apiResponse.close();
            } catch (Exception e) {

            }

        }

    }


}
