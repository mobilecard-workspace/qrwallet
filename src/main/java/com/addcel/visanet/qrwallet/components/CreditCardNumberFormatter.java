package com.addcel.visanet.qrwallet.components;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class CreditCardNumberFormatter {

    private final Logger LOGGER = LogManager.getLogger(CreditCardNumberFormatter.class);


    public static final String CREDIT_CARD_NUMBER_MASK = "************";

    public String maskedCardNumber(String tdcNumber) {
        if (tdcNumber == null) {
            tdcNumber = "";
        }
        return  CREDIT_CARD_NUMBER_MASK + (tdcNumber.length() < 4 ? tdcNumber : tdcNumber.substring(tdcNumber.length() - 4));
    }

}
