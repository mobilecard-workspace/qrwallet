package com.addcel.visanet.qrwallet.components;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class StringDecoder {

    private final Logger LOGGER = LogManager.getLogger(StringDecoder.class);

    public StringDecoder() {
        super();
    }
    
    public String hexNibblesToSString(String hexText) {
        if (hexText == null) {
            return null;
        }
        
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hexText.length() ; i += 2) {
            String code = hexText.substring(i, i + 2);
            char c = (char) Integer.valueOf(code,16).intValue();
            sb.append(c);
        }
        return sb.toString();
    }
}
