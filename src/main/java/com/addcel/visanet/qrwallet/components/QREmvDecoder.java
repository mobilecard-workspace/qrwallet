package com.addcel.visanet.qrwallet.components;

import com.addcel.visanet.qrwallet.dto.DecodeQrResponse;
import com.addcel.visanet.qrwallet.dto.DecodedQrContentDTO;
import com.addcel.visanet.qrwallet.dto.QrTagDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class QREmvDecoder {

    private final Logger LOGGER = LogManager.getLogger(QREmvDecoder.class);

    @Autowired
    private StringDecoder stringDecoder;

    @Autowired
    private Environment env;

    private QrTagDTO loadTag(final String qrCode, final int pos) throws Exception {
        LOGGER.info("QREmvDecoder.loadTag- init pos=" + pos);
        if (qrCode == null || qrCode.length() < pos + 4) {
            return null;
        }
        int p1 = pos + 2;
        int p2 = pos + 4;
        final String code = qrCode.substring(pos, p1);
        final int length = Integer.parseInt(qrCode.substring(p1, p2), 10);
        int p3 = p2 + length;
        if (qrCode.length() < p3) {
            return null;
        }
        final String content = qrCode.substring(p2, p3);
        LOGGER.info("QREmvDecoder.loadTag =" + code + ", length=" + length + ", content=" + content);
        return new QrTagDTO(pos, p2 + length, code, content);
    }


    public DecodedQrContentDTO decodeQrCodePlain(String qrContent) throws Exception {
        LOGGER.info("QREmvDecoder.decodeQrCodePlain - begin");

        int length = qrContent == null ? 0 : qrContent.length();
        if (length < 4 || !qrContent.startsWith("00")) {
            LOGGER.info("QREmvDecoder.decodeQrCodePlain - Exception invalid qr content ");
            throw new Exception("QR code is invalid");
        }

        int pos = 0;
        final DecodedQrContentDTO qr = new DecodedQrContentDTO();
        QrTagDTO currentTag = null;
        do {
            currentTag = loadTag(qrContent, pos);
            LOGGER.info("QREmvDecoder.decodeQrCodePlain - tagLoaded=" + currentTag);

            if (currentTag != null) {
                pos = currentTag.getEndPos();
                String code = currentTag.getCode();

                if (code.equals(env.getProperty("emv.qr.tag.currency.code"))) {
                    qr.setCurrencyCode(currentTag.getContent());
                } else if (code.equals(env.getProperty("emv.qr.tag.country.code"))) {
                    qr.setCountryCode(currentTag.getContent());
                } else if (code.equals(env.getProperty("emv.qr.tag.business.name"))) {
                    qr.setBusinessName(currentTag.getContent());
                } else if (code.equals(env.getProperty("emv.qr.tag.city.name"))) {
                    qr.setCity(currentTag.getContent());
                } else if (code.equals(env.getProperty("emv.qr.version"))) {
                    try {
                        qr.setVersion(Integer.parseInt(currentTag.getContent(), 10));
                    } catch (NumberFormatException e) {
                        LOGGER.info("QREmvDecoder.decodeQrCodePlain - error parsing qr version content=" + currentTag.getContent());
                        qr.setVersion(-1);
                    }
                } else if (code.equals(env.getProperty("emv.qr.type"))) {
                    final String content = currentTag.getContent();
                    if (content.equals(env.getProperty("visanet.qr.type.static.code"))) {
                        qr.setType(env.getProperty("visanet.qr.type.static"));
                    } else if (content.equals(env.getProperty("visanet.qr.type.dynamic.code"))) {
                        qr.setType(env.getProperty("visanet.qr.type.dynamic"));
                    } else {
                        LOGGER.info("QREmvDecoder.decodeQrCodePlain - error parsing qr version content=" + content);

                    }
                } else if (code.equals(env.getProperty("emv.qr.merchant.id"))) {
                    qr.setMerchantId(currentTag.getContent());
                } else if (code.equals(env.getProperty("emv.qr.id"))) {
                    qr.setId(currentTag.getContent());
                } else if (code.equals(env.getProperty("emv.qr.tag.transaction.amount"))) {
                    final String content = currentTag.getContent();
                    try {
                        qr.setAmount(new BigDecimal(content));
                    } catch (Exception e) {
                        LOGGER.info("QREmvDecoder.decodeQrCodePlain - error parsing qr version amount=" + content);
                    }
                } else {
                    // código no tomado en cuenta aún
                }
            }

        } while (currentTag != null);

        if (qr.getAmount() == null) {
            qr.setAmount(BigDecimal.ZERO);
        }
        LOGGER.info("QREmvDecoder.decodeQrCodePlain - finish method qr=" + qr);

        return qr;

    }

    public DecodedQrContentDTO decodeQrCodeHex(String qrHexadecimal) throws Exception {
        LOGGER.info("QREmvDecoder.decodeQrCodeHex - begin");
        DecodedQrContentDTO result =  decodeQrCodePlain(stringDecoder.hexNibblesToSString(qrHexadecimal));
        if (result.getAmount() == null) {
            result.setAmount(BigDecimal.ZERO);
        }
        return result;
    }

}

