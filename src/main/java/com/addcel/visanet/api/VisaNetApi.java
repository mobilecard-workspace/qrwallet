package com.addcel.visanet.api;

import com.addcel.visanet.api.dto.CardAutorizationRequest;
import com.addcel.visanet.api.dto.CardAutorizationResponse;
import com.addcel.visanet.api.dto.GenerateQrRequest;
import com.addcel.visanet.api.dto.GenerateQrResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.nio.charset.Charset;

@Component
public class VisaNetApi {

    @Autowired
    Environment env;

    public VisaNetApi() {
        super();
    }


    public String generateLoginToken() throws Exception {
        String url = env.getProperty("visanet.api.login.endpoint");
        System.out.println("URL a consumir  = " + url);
        CloseableHttpResponse apiResponse = null;
        CloseableHttpClient httpClient = null;
        InputStream responseContent = null;

        try {
            httpClient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(url);
            httpGet.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION, "Basic " + env.getProperty("visanet.api.login.auth.basic.token")));
            apiResponse = httpClient.execute(httpGet);
            System.out.println("Codigo de response " + apiResponse.getStatusLine().getStatusCode() + ": " + apiResponse.getStatusLine().getReasonPhrase());
            if (apiResponse.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
                responseContent = apiResponse.getEntity().getContent();
                return IOUtils.toString(responseContent, Charset.defaultCharset());
            } else {
                throw new Exception(" Error servicio login codigo=" + apiResponse.getStatusLine().getStatusCode() + ", descripcion=" + apiResponse.getStatusLine().getReasonPhrase());
            }
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
            if (apiResponse != null) {
                apiResponse.close();
            }

            if (responseContent != null) {
                responseContent.close();
            }
        }

    }

    public GenerateQrResponse generateQr(final String authToken, final GenerateQrRequest request) throws Exception {
        String url = env.getProperty("visanet.api.generateqr.endpoint");
        System.out.println("URL a consumir  = " + url);
        CloseableHttpResponse apiResponse = null;
        CloseableHttpClient httpClient = null;
        InputStream responseContent = null;

        try {
            httpClient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);

            ObjectMapper mapper = new ObjectMapper();
            httpPost.setEntity(new StringEntity(mapper.writeValueAsString(request)));
            httpPost.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION, authToken));
            httpPost.addHeader(new BasicHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE));
            httpPost.addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE));
            apiResponse = httpClient.execute(httpPost);

            System.out.println("Codigo de response " + apiResponse.getStatusLine().getStatusCode() + ": " + apiResponse.getStatusLine().getReasonPhrase());

            if (apiResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return mapper.readValue(apiResponse.getEntity().getContent(), GenerateQrResponse.class);
            } else {
                throw new Exception(" Error servicio login codigo=" + apiResponse.getStatusLine().getStatusCode() + ", descripcion=" + apiResponse.getStatusLine().getReasonPhrase());
            }
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
            if (apiResponse != null) {
                apiResponse.close();
            }

            if (responseContent != null) {
                responseContent.close();
            }
        }
    }

    public CardAutorizationResponse authorizePlainCard(final String authToken, final CardAutorizationRequest request) throws Exception {
        System.out.println("VisaNetApi.authorizePlainCard - inicio método request=" + request);
        String url = env.getProperty("visanet.api.athorize.plaincard.endpoint").trim();
        System.out.println("VisaNetApi.authorizePlainCard - URL a consumir  = " + url);
        CloseableHttpResponse apiResponse = null;
        CloseableHttpClient httpClient = null;

        try {
            httpClient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);
            ObjectMapper mapper = new ObjectMapper();
            httpPost.setEntity(new StringEntity(mapper.writeValueAsString(request)));
            System.out.println("VisaNetApi.authorizePlainCard - RequestBody= " + mapper.writeValueAsString(request));
            httpPost.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION, authToken));
            httpPost.addHeader(new BasicHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE));
            httpPost.addHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE));
            apiResponse = httpClient.execute(httpPost);
            System.out.println("VisaNetApi.authorizePlainCard - Codigo de response " + apiResponse.getStatusLine().getStatusCode() + ": " + apiResponse.getStatusLine().getReasonPhrase());

            if (apiResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                CardAutorizationResponse response = mapper.readValue(apiResponse.getEntity().getContent(), CardAutorizationResponse.class);
                System.out.println("VisaNetApi.authorizePlainCard - fin metodo método retorno codigo=" + (response == null ? null : (response.getCodeResponse() + " " + response.getMessage())));
                return response;
            } else {
                System.out.println("VisaNetApi.authorizePlainCard - fin metodo método exception con code=" + apiResponse.getStatusLine().getStatusCode() + ", descripcion=" + apiResponse.getStatusLine().getReasonPhrase());
                throw new Exception(" Error servicio autorizacion codigo=" + apiResponse.getStatusLine().getStatusCode() + ", descripcion=" + apiResponse.getStatusLine().getReasonPhrase());
            }
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
            if (apiResponse != null) {
                apiResponse.close();
            }

        }
    }
}
