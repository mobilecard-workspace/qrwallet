package com.addcel.visanet.api.dto;

import java.io.Serializable;

public class GenerateQrResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long codeResponse;

    private String message;

    private String tagId;

    private String tagImg;

    public GenerateQrResponse() {
        super();
    }
    
    public GenerateQrResponse(Long codeResponse, String message, String tagId, String tagImg) {
        super();
        this.codeResponse = codeResponse;
        this.message = message;
        this.tagId = tagId;
        this.tagImg = tagImg;
    }

    public Long getCodeResponse() {
        return codeResponse;
    }

    public void setCodeResponse(Long codeResponse) {
        this.codeResponse = codeResponse;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTagImg() {
        return tagImg;
    }

    public void setTagImg(String tagImg) {
        this.tagImg = tagImg;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GenerateQrResponse{");
        sb.append("codeResponse=").append(codeResponse);
        sb.append(", message='").append(message).append('\'');
        sb.append(", tagId='").append(tagId).append('\'');
        sb.append(", tagImg='").append(tagImg).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
