package com.addcel.visanet.api.dto;

import java.io.Serializable;

public class CardAutorizationResult implements Serializable {

    private static final long serialVersionUID = 1L;

    private CardAuthorizationHeader header;

    private CardAuthorizationOrder order;

    private CardAuthorizationDataMap dataMap;

    public CardAutorizationResult () {

    }

    public CardAutorizationResult(CardAuthorizationHeader header, CardAuthorizationOrder order, CardAuthorizationDataMap dataMap) {
        this.header = header;
        this.order = order;
        this.dataMap = dataMap;
    }

    public CardAuthorizationHeader getHeader() {
        return header;
    }

    public void setHeader(CardAuthorizationHeader header) {
        this.header = header;
    }

    public CardAuthorizationOrder getOrder() {
        return order;
    }

    public void setOrder(CardAuthorizationOrder order) {
        this.order = order;
    }

    public CardAuthorizationDataMap getDataMap() {
        return dataMap;
    }

    public void setDataMap(CardAuthorizationDataMap dataMap) {
        this.dataMap = dataMap;
    }
}
