package com.addcel.visanet.api.dto;

import java.io.Serializable;

public class CardAutorizationResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String codeResponse;

    private String message;

    private CardAutorizationResult result;

    public CardAutorizationResponse () {
        super();
    }

    public CardAutorizationResponse(String codeResponse, String message, CardAutorizationResult result) {
        this.codeResponse = codeResponse;
        this.message = message;
        this.result = result;
    }

    public String getCodeResponse() {
        return codeResponse;
    }

    public void setCodeResponse(String codeResponse) {
        this.codeResponse = codeResponse;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CardAutorizationResult getResult() {
        return result;
    }

    public void setResult(CardAutorizationResult result) {
        this.result = result;
    }
}
