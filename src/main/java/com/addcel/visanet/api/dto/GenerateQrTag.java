package com.addcel.visanet.api.dto;

import java.io.Serializable;

public class GenerateQrTag implements Serializable {

    private static final long serialVersionUID = 1L;

    private String tag;

    private String value;

    public GenerateQrTag() {
        super();
    }

    public GenerateQrTag(String tag, String value) {
        this.tag = tag;
        this.value = value;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
