package com.addcel.visanet.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GenerateQrComp implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private List<GenerateQrTag> tags;

    public GenerateQrComp () {
        super();
        tags = new ArrayList<>();
    }

    public GenerateQrComp(String name) {
        super();
        this.name = name;
        this.tags = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GenerateQrTag> getTags() {
        return tags;
    }

    public void setTags(List<GenerateQrTag> tags) {
        this.tags = tags;
    }
}
