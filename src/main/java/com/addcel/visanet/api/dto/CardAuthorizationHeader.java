package com.addcel.visanet.api.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardAuthorizationHeader implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("ecoreTransactionUUID")
    private String ecoreTransactionUuid;

    @JsonProperty("ecoreTransactionDate")
    private String ecoreTransactionDate;

    @JsonProperty("millis")
    private String millis;

    public CardAuthorizationHeader () {

    }

    public CardAuthorizationHeader(String ecoreTransactionUUID, String ecoreTransactionDate, String millis) {
        this.ecoreTransactionUuid = ecoreTransactionUUID;
        this.ecoreTransactionDate = ecoreTransactionDate;
        this.millis = millis;
    }

    public String getEcoreTransactionUuid() {
        return ecoreTransactionUuid;
    }

    public void setEcoreTransactionUuid(String ecoreTransactionUuid) {
        this.ecoreTransactionUuid = ecoreTransactionUuid;
    }

    public String getEcoreTransactionDate() {
        return ecoreTransactionDate;
    }

    public void setEcoreTransactionDate(String ecoreTransactionDate) {
        this.ecoreTransactionDate = ecoreTransactionDate;
    }

    public String getMillis() {
        return millis;
    }

    public void setMillis(String millis) {
        this.millis = millis;
    }

   
}
