package com.addcel.visanet.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CardAutorizationRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("cardNumber")
    private String cardNumber;

    @JsonProperty("cvv2Code")
    private String cvv2Code;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("createAlias")
    private String createAlias;

    @JsonProperty("currencyId")
    private String currencyId;

    @JsonProperty("email")
    private String email;

    @JsonProperty("installments")
    private String installments;

    @JsonProperty("expirationMonth")
    private String expirationMonth;

    @JsonProperty("expirationYear")
    private String expirationYear;

    @JsonProperty("externalTransactionId")
    private String externalTransactionId;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("merchantid")
    private String merchantId;

    @JsonProperty("qremv")
    private String qremv;

    @JsonProperty("purchaseNumber")
    private String purchaseNumber;

    public CardAutorizationRequest () {

    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvv2Code() {
        return cvv2Code;
    }

    public void setCvv2Code(String cvv2Code) {
        this.cvv2Code = cvv2Code;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCreateAlias() {
        return createAlias;
    }

    public void setCreateAlias(String createAlias) {
        this.createAlias = createAlias;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInstallments() {
        return installments;
    }

    public void setInstallments(String installments) {
        this.installments = installments;
    }

    public String getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

    public String getExternalTransactionId() {
        return externalTransactionId;
    }

    public void setExternalTransactionId(String externalTransactionId) {
        this.externalTransactionId = externalTransactionId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getQremv() {
        return qremv;
    }

    public void setQremv(String qremv) {
        this.qremv = qremv;
    }

    public String getPurchaseNumber() {
        return purchaseNumber;
    }

    public void setPurchaseNumber(String purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CardAutorizationRequest{");
        sb.append("cardNumber='").append(cardNumber).append('\'');
        sb.append(", cvv2Code='").append(cvv2Code).append('\'');
        sb.append(", amount='").append(amount).append('\'');
        sb.append(", createAlias='").append(createAlias).append('\'');
        sb.append(", currencyId='").append(currencyId).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", installments='").append(installments).append('\'');
        sb.append(", expirationMonth='").append(expirationMonth).append('\'');
        sb.append(", expirationYear='").append(expirationYear).append('\'');
        sb.append(", externalTransactionId='").append(externalTransactionId).append('\'');
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", merchantId='").append(merchantId).append('\'');
        sb.append(", qremv='").append(qremv).append('\'');
        sb.append(", purchaseNumber='").append(purchaseNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
