package com.addcel.visanet.api.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardAuthorizationOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("purchaseNumber")
    private String purchaseNumber;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("installment")
    private String installment;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("externalTransactionId")
    private String externalTransactionId;

    @JsonProperty("authorizedAmount")
    private String authorizedAmount;

    @JsonProperty("authorizationCode")
    private String authorizationCode;

    @JsonProperty("actionCode")
    private String actionCode;

    @JsonProperty("traceNumber")
    private String traceNumber;

    @JsonProperty("transactionDate")
    private String transactionDate;

    @JsonProperty("transactionId")
    private String transactionId;

    public CardAuthorizationOrder() {

    }

    public String getPurchaseNumber() {
        return purchaseNumber;
    }

    public void setPurchaseNumber(String purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getCurrency() {
        return currency;
    }

    public String getExternalTransactionId() {
        return externalTransactionId;
    }

    public void setExternalTransactionId(String externalTransactionId) {
        this.externalTransactionId = externalTransactionId;
    }

    public String getAuthorizedAmount() {
        return authorizedAmount;
    }

    public void setAuthorizedAmount(String authorizedAmount) {
        this.authorizedAmount = authorizedAmount;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getTraceNumber() {
        return traceNumber;
    }

    public void setTraceNumber(String traceNumber) {
        this.traceNumber = traceNumber;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
