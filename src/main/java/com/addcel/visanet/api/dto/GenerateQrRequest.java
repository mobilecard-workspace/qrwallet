package com.addcel.visanet.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GenerateQrRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean enabled;

    private List<GenerateQrParam> param;

    private List<GenerateQrComp> comp;

    private String tagType;

    private String validityDate;

    public GenerateQrRequest () {
        super ();
        this.param = new ArrayList<>();
        this.comp = new ArrayList<>();
    }

    public GenerateQrRequest(boolean enabled, String tagType, String validityDate) {
        super ();
        this.enabled = enabled;
        this.tagType = tagType;
        this.validityDate = validityDate;
        this.param = new ArrayList<>();
        this.comp = new ArrayList<>();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<GenerateQrParam> getParam() {
        return param;
    }

    public void setParam(List<GenerateQrParam> param) {
        this.param = param;
    }

    public List<GenerateQrComp> getComp() {
        return comp;
    }

    public void setComp(List<GenerateQrComp> comp) {
        this.comp = comp;
    }

    public String getTagType() {
        return tagType;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }

    public String getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(String validityDate) {
        this.validityDate = validityDate;
    }
}
