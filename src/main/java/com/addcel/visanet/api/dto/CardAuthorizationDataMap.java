package com.addcel.visanet.api.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardAuthorizationDataMap implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("idunico")
    private String idUnico;
    
    @JsonProperty("merchant")
    private String merchant;
    
    @JsonProperty("authorizationcode")
    private String authCode;
    
    @JsonProperty("card")
    private String card;
    
    @JsonProperty("currency")
    private String currency;
    
    @JsonProperty("terminal")
    private String terminal;
    
    @JsonProperty("transactiondate")
    private String transactionDate;
    
    @JsonProperty("actioncode")
    private String actionCode;
    
    @JsonProperty("tracenumber")
    private String traceNumber;
    
    @JsonProperty("status")
    private String status;
    
    @JsonProperty("actiondescription")
    private String actionDescription;
    
    @JsonProperty("adquirente")
    private String adquiriente;
    
    @JsonProperty("amount")
    private String amount;
    
    @JsonProperty("processcode")
    private String processCode;
    
    @JsonProperty("transactionid")
    private String transactionId;
    
    public CardAuthorizationDataMap () {
        
    }

    public String getIdUnico() {
        return idUnico;
    }

    public void setIdUnico(String idUnico) {
        this.idUnico = idUnico;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getTraceNumber() {
        return traceNumber;
    }

    public void setTraceNumber(String traceNumber) {
        this.traceNumber = traceNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }

    public String getAdquiriente() {
        return adquiriente;
    }

    public void setAdquiriente(String adquiriente) {
        this.adquiriente = adquiriente;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    
    
    
}
