package com.addcel.visanet.qrwallet.application;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
//@ContextConfiguration(classes= {VisaQrWalletServiceApplication.class,})
public class VisaQrWalletServiceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
